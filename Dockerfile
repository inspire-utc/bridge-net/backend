FROM python:3.10 AS base

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

ENV PIPENV_VENV_IN_PROJECT 1

RUN apt update
RUN apt install -y gdal-bin libgdal-dev

WORKDIR /app

# Install pipenv and compilation dependencies
RUN pip install pipenv
COPY . .
RUN cd /app && pipenv install --deploy

FROM base AS runtime
ENV PATH "/app/.venv/bin:$PATH"
WORKDIR /app/bridgenet
RUN pip3 install gunicorn
ENTRYPOINT [ "python3", "manage.py", "runserver", "0.0.0.0:8000" ]
