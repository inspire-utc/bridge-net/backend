alias rmetric='docker compose restart compute-metric-worker'
alias lmetric='docker compose logs --tail=100 compute-metric-worker'
alias lserv='docker compose logs --tail=100 backend'