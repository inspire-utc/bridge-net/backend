echo "Building BridgeNet Backend Docker"
echo "-----------------------"

docker build -t bridgenet-backend .

echo "Building Bridgenet Frontend Docker"
echo "----------------------------------"

pushd ./bridgenet/web
docker build -t "bridgenet-frontend" .
popd
