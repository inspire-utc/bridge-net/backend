Inspection Tool
===============

.. contents::
   :local:

Overview
--------

The inspection tool allows you to tag images and videos according to AASHTO standards. Each Image contains elements, and the elements are inspected by tagging it with the following attributes

- Condition

- Environment

- Defect

- Code

To perform visual inspection on an image, follow these steps:


Image
-----

1. Navigate to the Visual Bridge Inspection Tab

.. image:: static/inspection_tab.png

2. Select the Bridge, Element, and Media. First we will select Image

.. image:: static/inspection_image.png

3. Note that the structural element has been outlined in Red. Fill out the form and press submit. Once the result has been saved, a green highlight will appear around the form

.. image:: static/inspection_performed.png

4. This information can be viewed in tabular form in the Archive Condition table

.. image:: static/inspection_archive.png

Video
-----

Inspecting a video is very similar, except we select Video Media

.. image:: static/inspection_video.png


If the video has been indexed for structural elements and the video has the element, it will show up. Only one video will show up per page.

