Workflow Engine
===============
.. contents::
   :local:

Overview
--------

The Workflow Orchestrator allows you to create custom Extract-Transform-Load (ETL) pipelines for ML models. For example, suppose you have a video that has a fisheye effect due to a wide angle lens
You can create a workflow that rectifies the fisheye distortion. The Workflow Orchestrator allows you to configure video pre-processing steps as well. For example, splitting the video into frames.
You may execute a workflow, then inspect the result of a given node (if that node provides some sort of viewable result).

Features
--------

- User configurable workflow
- Multiple image and video processing algorithms to chose from
- Inspectable results

Usage
-----

Lets go through the process of indexing a video to as an example of how to create and inspect a workflow. The goal of this exercise is to create an index so that a video can be searched by structucal element. As always, make sure you are logged in to BridgeNet.

.. image:: /_static/user_logged_in.png

1. Navigate to the Workflow Panel and click the '+' Button

.. image:: static/Workflow.png

2. You will be greeted with the Workflow UI

.. image:: static/WorkflowUI.png

The UI features the following sections:
- Work Items : contains nodes that the user can add to the workflow

- Save : Save any edits to the current workflow

- Save As : Save a copy of this workflow - When the user creates a new workflow for the first time, they must first click Save As

- Delete Node : When the user clicks on a node, they can chose to delete it.

- Delete Edge: When the user clicks on a edge, they can chose to delete it

- Vertical Layout : Automatically prettify the graph by aligning the nodes vertically

- Horizontal Layout : Automatically prettify the graph by aligning the nodes horizontally

- Configure Node : Each node has specific settings. When the user clicks a node, clicking this button will bring up a panel that allows the user to configure the node.

- Activate Workflow : Activate this workflow - The workflow must be saved first


3. Lets arrange the node in such a way that allows the user to analyse a video every 10 frames, then index the video.

.. image:: static/index_configuration.png

It's important to note that not every configuration is valid. The system also won't tell you if a configuration is invalid, which is unfortunate.

4. Then we select the 'Split Video Node' and configure which video we want to process. Make sure you've already uploaded a video and assigned it to a bridge first!

.. image:: static/SplitVideo.png

The dropdown will automatically populate with available bridges, available videos. Select the number of frames you want to extract. So if you  'Extract every N Seconds' is 10, we extract the frames at at second 1, 10, 20, 30. The lower the number, the more frames we have.

5. Save the Workflow, then Activate the workflow! Navigate to the 'Monitor' tab, which gives you a list of all workflows ran. Unfortunatly, I haven't implemented a way to quickly navigate to the workflow you just started, or a way to search workflows, so you just have to find it by navigating by timestamp.

.. image:: static/monitor.png

6. Once you click on it, you will be able to see the progress of the workflow

.. image:: static/workflow_progress.png

The color indicates status of the node.

- Green : Complete

- Blue : In Progress

- Yellow : Scheduled

- Red : ERROR


7. This page has the Vertical and Horizontal Layout feature, as well as a "View Result" button. You need to select the node you want to inspect in order to view result. When you inspect the result of the Mask RCNN workflow, you can see the predictions that the model made.

.. image:: static/mask_rcnn_index_result.png



Additional Notes
----------------

Any additional information, tips, or references related to the **Workflow Engine**.

