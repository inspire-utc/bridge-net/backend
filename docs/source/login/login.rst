Login
=====

This section covers the **Login**.

.. contents::
   :local:

Overview
--------

This section shows how to login to BridgeNet

Usage
-----

1. Click on the login button to bring up the Login form

.. image:: static/login_button.png

2. Enter your username and password

.. image:: static/login_screenshot.png
    
Known Issues
------------

There currently is not a way to create a user. Please talk to Kevin for admin user and password


