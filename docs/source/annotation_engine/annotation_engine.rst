Annotation Tool
===============

.. contents::
   :local:

Overview
--------

The Annotation tool allows you to annotate parts of the image. This is important for evaluating models by providing ground truth data. BridgeNet has a built in annotator. This document will walk you through the process of annotating polygons on a bridge image.

Usage
-----

1. Ensure you are logged in

.. image:: /_static/user_logged_in.png

2. Navigate to the annotation tab and select the Annotate Tab. Then select the Bridge. As a reminder, the Bridge is a dataset of imags.

.. image:: static/annotate_nav.png

3. Once you click on the bridge, it will bring you to the Annotator tool. To create an annotation, we must first create a Name attribute. Start by opening the Attribute tab.

.. image:: static/annotate_attribute.png

4. Add a 'Name' attribute and press the '+' Button. 

.. image:: static/annotate_attribute_name.png

5. Now we are ready to start annotating. Make sure to review the 'Keyboard Shortcut' tab for a brief tutorial on how to annotate. Note that we can only use the following drawing tools:

.. image:: static/annotate_polygon_tools.png

I personally prefer using the 'Polygon' tool, which is the closed polygon on the left because the polygon is always closed. The polyline tool, you need to manually close the polygon

6. Draw and annotate the polygon - Click the image you want to annotate. Then click somewhere on the image to draw your polygon. Once you are done drawing the polygon, press Escape, which will take you out of drawing mode. Click on the polygon you just drew and a box will show up. Enter the name of the structural component

.. image:: static/annotate_and_name.png

7. Once you are done, click on Annotation,  then Post Annotation to BridgeNet

.. image:: static/annotate_save_annotation.png

