Video Query
===========

.. contents::
   :local:

Overview
--------

The Video Query feature allows you to query a video by bridge element. For example, I want to go to the part of the video with a 'pier' element. Instead of manually seeking through the video trying to find piers, the Video Query will do it for you.
Before a video can be queried, it first must be indexed. See `workflow engine </workflow_engine/workflow_engine.html>`_ to create a Video Index Workflow.

Usage
-----

1. Navigate to the Video Gallery in the Archive and Click on Query Video Button

.. image:: static/query_video.png

2. Once you click on Query Video, you should see a table with the elements you want to view

.. image:: static/query_video_table.png

You can click on the button and it will take you to the part of the video with that element

Additional Notes
----------------

The video takes a long time to load. In hindsight, I should have added a step that downsamples the video (brings down the resolution) so it loads faster.

