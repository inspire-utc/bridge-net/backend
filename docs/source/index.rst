.. BridgeNet documentation master file, created by
   sphinx-quickstart on Wed Oct 23 02:59:38 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BridgeNet Learning Modules
==========================

This document provides information on how to use BridgeNet. It is a platform that hosts bridge inspection data, annotation data for deep learning model training, and a workflow engine that allows inspectors to run their machine learning models. We hope that this platform helps train bridge inspectors to better perform their tasks. BridgeNet was developed by the Center for Intelligent Infrastructure with funding from US Department of Transportation



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   login/login
   upload_images/index
   annotation_engine/annotation_engine
   workflow_engine/workflow_engine
   video_query/video_query
   inspection_tool/inspection_tool
