Uploading Image and Video
=========================

.. contents::
   :local:

Overview
--------

Uploading Image and Video is a multistep process. First we must create a bridge instance, if one does not exist already. Then we assign each uploaded resource to that bridge.


Usage
-----

1. Make sure you are logged in.

.. image:: /_static/user_logged_in.png

Create Bridge Instance
^^^^^^^^^^^^^^^^^^^^^^

1. To create a bridge instance, click on the archive tab

.. image:: static/click_archive.png

2. Click on the Add Button

.. image:: static/click_add_image.png

3. Fill out the form and press submit - Remember the Structure Number!

.. image:: static/create_bridge_form.png


Upload Photo
^^^^^^^^^^^^

1. To upload photo - Click on Archive Tab

.. image:: static/click_archive.png

2. Click on Image tab

.. image:: static/image_tab.png

3. Click on Add Button

.. image:: static/click_add_image.png

4. Upload Image - Make sure you manually type out the entire structure number from the last step. There is a bug where it won't work if you select it from the dropdown

.. image:: static/submit_image_form.png

Upload Video
^^^^^^^^^^^^

Uploading a video is the same procedure, except you click the 'Video' tab
