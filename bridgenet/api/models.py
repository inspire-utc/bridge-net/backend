""" Base definition file of all models. If you are going to change the data stores in a model, do it here then propogate it to the other files.
"""

from django.db import models, transaction
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
import uuid
import os
import shutil
import logging
import enum

logger = logging.getLogger(__name__)

states = {
    "AK": 1,
    "AL": 2,
    "AR": 5,
    "AZ": 4,
    "CA": 6,
    "CO": 8,
    "CT": 9,
    "DC": 11,
    "DE": 10,
    "FL": 12,
    "GA": 13,
    "HI": 15,
    "IA": 19,
    "ID": 16,
    "IL": 17,
    "IN": 18,
    "KS": 20,
    "KY": 21,
    "LA": 22,
    "MA": 25,
    "MD": 24,
    "ME": 23,
    "MI": 26,
    "MN": 27,
    "MO": 29,
    "MS": 28,
    "MT": 30,
    "NC": 37,
    "ND": 38,
    "NE": 31,
    "NH": 33,
    "NJ": 34,
    "NM": 35,
    "NV": 32,
    "NY": 36,
    "OH": 39,
    "OK": 40,
    "OR": 41,
    "PA": 42,
    "RI": 44,
    "SC": 45,
    "SD": 46,
    "TN": 47,
    "TX": 48,
    "UT": 49,
    "VA": 51,
    "VT": 50,
    "WA": 53,
    "WI": 55,
    "WV": 54,
    "WY": 56,
}
# Create your models here.


# This is a custom user model that overrides the default in Django.
# It is very challenging to make a custom user after production, so we did it early.
# If users need some data associated with them, you can assign it here
class User(AbstractUser):
    email = models.EmailField(blank=False, max_length=254, verbose_name="email address")

    USERNAME_FIELD = "username"  # e.g: "username", "email"
    EMAIL_FIELD = "email"  # e.g: "email", "primary_email"


# This overwrites rest_frameworks default token to allow multiple tokens per user
# NOTE might be worth settings up token expiration but that would involve using
#   a job manager of some kind like celery. Might be worth looking into closer to production.
#   It also might be worth
class MultiToken(Token):
    user = models.ForeignKey(  # default uses OneToOne relationship, we changed it to ForeignKey to allow users to have multiple
        settings.AUTH_USER_MODEL,
        related_name="tokens",
        on_delete=models.CASCADE,
        verbose_name=("User"),
    )


# This is a special signal that recieves when a user is saved and creates a token
# NOTE multitoken now creates a token during every login
# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def create_auth_token(sender, instance=None, created=False, **kwargs):
#     if created:
#         Token.objects.create(user=instance)


class Bridge(models.Model):
    state_number = models.PositiveSmallIntegerField(
        blank=True
    )  # federally administered state number
    structure_number = models.PositiveIntegerField(
        unique=True, primary_key=True
    )  # meant to mimic structure like fhwa uses
    county_name = models.CharField(max_length=200)
    year_built = models.PositiveSmallIntegerField()
    state_code = models.CharField(max_length=2)
    structure_type = models.CharField(
        max_length=2,
        choices=[("SG", "steel girder"), ("CG", "pre-sress concrete girder")],
    )
    is_ltbp = (
        models.BooleanField()
    )  # whether or not bridge is Long-Term Bridge Performance tracked

    def save(self, *args, **kwargs):
        self.state_number = states[self.state_code]
        super().save(*args, **kwargs)


class Element(models.Model):
    id = models.CharField(max_length=200, primary_key=True, editable=False)
    name = models.CharField(max_length=200, null=True, blank=True)  # just a plain name
    element_number = models.IntegerField()  # as defined by the fhwa national bridge inventory and AASHTO
    parent_element = models.ForeignKey(
        "Element",
        related_name="child_elements",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )  # any element may have a parent element. This allows for a tree of bridge parts (ie structure->element->component)

    # This won't necessarily be used but it looks better if using stringRelatedField in serializers.py
    def __str__(self):
        return f"{self.name}"


class Condition(models.Model):

    class E_Environment(models.Choices):
        BENIGN = "Benign"
        LOW = "Low"
        MODERATE = "Moderate"
        SEVERE = "Severe"
    
    class E_Condition(models.Choices):
        CS1 = "CS1"
        CS2 = "CS2"
        CS3 = "CS3"
        CS4 = "CS4"
    
    

    type = models.CharField(null=True, max_length=200)  # ie cracks
    location = models.CharField(null=True, max_length=200)
    severity = models.CharField(null=True, max_length=200)  # How bad is it
    length = models.CharField(
        null=True,
        max_length=200
    )  # Useful for any measurement of condition taken
    units = models.CharField(null=True, max_length=200)  # unit of measurement taken
    comment = models.TextField(null=True,blank=True)  # optional extra info
    element = models.ForeignKey(
        Element, related_name="conditions", on_delete=models.CASCADE
    )  # element this condition belongs to

    image = models.ForeignKey(
        "Image", related_name="conditions", on_delete=models.CASCADE, null=True
    )  # element this condition belongs to 

    defect = models.BooleanField(null=True)
    environment = models.CharField(null=True, choices=E_Environment.choices, max_length=20)
    condition = models.CharField(null=True, choices=E_Condition.choices, max_length=20)
    code = models.IntegerField(null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["image", "element"], name="unique_image_element"
            )
        ]

def createVideoPath(instance, filename):
    fullPath = "videos/"
    if instance.bridge:
        fullPath += str(instance.bridge.structure_number) + "/"
    return fullPath + filename


class Video(models.Model):
    bridge = models.ForeignKey(
        Bridge, related_name="videos", on_delete=models.SET_NULL, null=True, blank=True
    )  # when the bridge this image is related to is deleted, just set its value to null
    file = models.FileField(upload_to=createVideoPath, null=False, blank=False)
    important_frames = ArrayField(models.IntegerField(), null=False, default=list)  # postgres specific

# used by image to format storage location. It is important
# to not have tons of files in one directory. Makes loading a specific
# file faster
def createImagePath(instance, filename):
    fullPath = "images/"
    if instance.bridge:
        fullPath += str(instance.bridge.structure_number) + "/"
    return fullPath + filename


class Image(models.Model):
    bridge = models.ForeignKey(
        Bridge, related_name="images", on_delete=models.SET_NULL, null=True, blank=True
    )  # when the bridge this image is related to is deleted, just set its value to null

    element = models.ForeignKey(
        Element,  
        related_name="images",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,)
    condition = models.ForeignKey(
        Condition,
        related_name="images",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    # This should be an absolute path from which the image stored
    file = models.ImageField(upload_to=createImagePath, null=False, blank=False)

    video = models.ForeignKey(
        Video, related_name="images", on_delete=models.SET_NULL, null=True, blank=True
    )  # optional, for if this image comes from a video
    frame_number = models.IntegerField(
        null=True, blank=True
    )  # should only be set if video is set

    # custom save overide that makes sure to propogate an image up its parent's tree.
    # This also catches some mistakes and assumes the most specific model specified is the correct model.
    # ie if a condition and bridge are specific, but the condition is not actually a grandchild of that bridge,
    # the bridge is corrected to be the grandfather of the condition specified.
    def save(self, *args, **kwargs):
        if self.condition:
            self.element = self.condition.element
        if self.element:
            # for element in self.element.element_set.all():
            self.bridge = self.element.bridge
        if not self.video:
            self.frame_number = None
        super().save(*args, **kwargs)

class ImageElement(models.Model):
    bridge = models.ForeignKey(Bridge, on_delete=models.CASCADE)
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    element = models.ForeignKey(Element, on_delete=models.CASCADE)
    # You can add additional fields here, such as a timestamp, relationship attributes, etc.

    class Meta:
        unique_together = ('image', 'element')  # 


# Image annotation, should be multiple annotations per image
class Annotation(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    region_name = models.CharField(max_length=100000)
    region_coord = models.CharField(max_length=100000)
    region_share = models.CharField(max_length=100000)
    image = models.ForeignKey(
        Image,
        related_name="annotations",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )  # if image is deleted annotation is deleted

    element = models.ForeignKey(
        Element,
        related_name="annotations",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    condition = models.ForeignKey(
        Condition,
        related_name="annotations",
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )





# Stores the graph representation of a workflow
class Workflow(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, default="New Workflow")
    nodes = models.JSONField()
    edges = models.JSONField()
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


class WorkflowJob(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, default="New Workflow") # doesn't do anyting
    nodes = models.JSONField()
    edges = models.JSONField()
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    associated_workflow = models.ForeignKey(Workflow, related_name="workflows", on_delete=models.SET_NULL, editable=False, null=True)

     

from django.utils import timezone
from datetime import timedelta

class WorkStatus(models.TextChoices):
    PENDING = "PENDING"
    RUNNING = "RUNNING"
    DONE = "DONE"
    ERROR = "ERROR"
    CANCELED = "CANCELED"

class WorkItem(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=255)
    status = models.CharField(choices=WorkStatus.choices, default=WorkStatus.PENDING, max_length=255)
    started_at = models.DateTimeField(null=True, blank=True)
    completed_at = models.DateTimeField(null=True, blank=True)
    input_data = models.JSONField(null=True, blank=True)
    result = models.JSONField(null=True,blank=True)
    error_message = models.JSONField(null=True, blank=True)
    associated_workflowjob = models.ForeignKey(WorkflowJob, on_delete=models.CASCADE, null=True, blank=True)
    

class Workspace(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, default="New Workspace")
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    associated_workitem = models.ForeignKey(WorkItem, on_delete=models.CASCADE, null=True, blank=True)
    associated_workflowjob = models.ForeignKey(WorkflowJob, on_delete=models.CASCADE, null=True, blank=True)
    workspace = models.FilePathField()

    # These attributes determine when a workspace is cleaned up
    last_released = models.DateTimeField(null=True, blank=True)
    time_delta = models.DurationField(default=timedelta(minutes=1440))
    semaphore = models.IntegerField(default=0)


    def absolute_worker_directory(self):
        # in the context of a worker, path is mounted to /media
        return "/media/workspaces/" + str(self.id) + "/"

    def init_workspace(self):
        base_path = "./media/workspaces/"
        self.workspace = base_path + str(self.id) + "/"
        if not os.path.exists(self.workspace):
            os.makedirs(self.workspace)
            logger.info("Workspace created")
        self.save()
    
    @transaction.atomic
    def delete(self, *args, **kwargs):
        # check that current time is greater than last_released + time_delta in an assertion
        assert self.semaphore == 0, "Cannot delete a workspace that is in use"
        assert self.last_released + self.time_delta < timezone.now()

        if os.path.exists(self.workspace):
            shutil.rmtree(self.workspace, ignore_errors=True)
        super().delete(*args, **kwargs)
        logger.info("Workspace deleted")

    @transaction.atomic
    def acquire(self):
        self.semaphore = 1
        self.save(update_fields=['semaphore'])
        logger.debug("Workspace acquired")

    @transaction.atomic
    def release(self):
        self.semaphore = 0
        self.last_released = timezone.now()
        self.save(update_fields=['semaphore', 'last_released'])
        logger.debug("Workspace released")

class VideoIndex(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    index = models.JSONField()


class FlatVideoIndex(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    bridge = models.ForeignKey(Bridge, on_delete=models.CASCADE)
    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    element = models.ForeignKey(Element, on_delete=models.CASCADE)
    frame_no = models.IntegerField()
    frame_sec = models.IntegerField()

