import typing
import time
from threading import Thread
from celery.result import AsyncResult
from api.models import WorkItem, WorkStatus, Workspace
import datetime
import uuid
import json

from api.workers.split_video import split_video
from api.workers.framediff import framediff
from api.workers.merge_frames import merge_frames
from api.workers.defisheye import defisheye_main
from api.workers.mock import mock
from api.workers.mask_rcnn_inference import mask_rcnn_inference
from api.workers.compute_metric import compute_metric
from api.workers.multitask_learning import multitask

from .models import VideoIndex, Video

import logging

logger = logging.getLogger(__name__)

if typing.TYPE_CHECKING:
    from .models import WorkflowJob

class WorkflowGraph:

    def __init__(self, nodes, edges):
        self.nodes = {}
        self.edges = {}

        for node in nodes:
            id = node["id"]
            self.nodes[id] = node 
        
        for edge in edges:
            id = edge["id"]
            self.edges[id] = edge
    
    def start_node(self):
        return self.nodes["1"]

    def next_nodes(self, node) -> list:
        next_nodes = []
        for edge in self.edges.values():
            source = edge["source"]
            target = edge["target"]
            if source == node["id"]:
                next_node = self.nodes.get(target, None)
                if next_node:
                    next_nodes.append(next_node)
        return next_nodes

    def prev_nodes(self, node) -> list:
        prev_nodes = []
        for edge in self.edges.values():
            source = edge["source"]
            target = edge["target"]
            if target == node["id"]:
                prev_node = self.nodes.get(source, None)
                if prev_node and prev_node["id"] != "1": # exclude start node
                    prev_nodes.append(prev_node)
        return prev_nodes
    

class WorkflowInterpreter:

    def __init__(self, workflowjob: "WorkflowJob"):
        self.workflowjob = workflowjob
        self.graph = WorkflowGraph(workflowjob.nodes, workflowjob.edges)
        for node in workflowjob.nodes:
            node["workitem_id"] = str(uuid.uuid4())
        workflowjob.save()

        


    def schedule_workitem(self, node) -> Thread:
        print("schedule node: ", node["data"], "Scheduled")
        # Input data must be dict
        input_data = node["data"].get("input_data", {})
        job_type = node["data"]["label"]

        # Create the worker database entry
        work_item = WorkItem.objects.create(
            id=uuid.UUID(node["workitem_id"]),
            type=job_type,
            input_data=input_data,
            started_at=datetime.datetime.now(),
            associated_workflowjob=self.workflowjob
        )

        work_item.save()

        node["work_item_id"] = str(work_item.id)

        def worker():
            """
            This is the thread worker. It waits for the result of the previous node, 
            then dispatches the worker to Celery. Then it waits for it to cocmplete
            """
            # Wait for all prev node to finish
            for prev in self.graph.prev_nodes(node):
                waiter:Thread = prev["waiter"]
                waiter.join()

            
            input_data = work_item.input_data = node["data"].get("input_data", {})
            work_item.status = WorkStatus.RUNNING
            work_item.save()

            workspace = Workspace.objects.create(
                associated_workitem=work_item,
                associated_workflowjob=self.workflowjob
            )

            workspace.init_workspace()
            workspace.acquire()
            node["workspace"] = workspace

            ignore_worker_result = None
            try:
                # Acquire all previous workspaces:
                for prev in self.graph.prev_nodes(node):
                    prev_workspace = prev["workspace"]
                    prev_workspace.acquire()

                # Start the worker
                if job_type == "Split Video":
                    task = split_video.split_video.delay(input_data, workspace.absolute_worker_directory())
                elif job_type == "Frame Diff":
                    task = framediff.framediff.delay(input_data, workspace.absolute_worker_directory())
                elif job_type == "Active Label Propagation":
                    task = framediff.framediff.delay(input_data, workspace.absolute_worker_directory()) # Mock
                elif job_type == "Merge Frames":
                    task = merge_frames.merge_frames.delay(input_data, workspace.absolute_worker_directory())
                elif job_type == "Defisheye":
                    task = defisheye_main.defisheye.delay(input_data, workspace.absolute_worker_directory())
                elif job_type == "Ground Truth":
                    print("Handling ground truth")
                    ignore_worker_result = handle_ground_truth(input_data)
                    task = mock.mock.delay(input_data, workspace.absolute_worker_directory())
                elif job_type == "Compute Metric":
                    task = compute_metric.compute_metric.delay(input_data, workspace.absolute_worker_directory())

                # -- MASK RCNN INFERENCE -- #
                elif job_type == "Mask RCNN Inference":
                    input_data = construct_mask_rcnn_input(input_data)
                    work_item.input_data = input_data
                    work_item.save()
                    task = mask_rcnn_inference.mask_rcnn_inference.delay(input_data, workspace.absolute_worker_directory())
                 
                elif job_type == "Mock Worker":
                    task = mock.mock.delay(input_data, workspace.absolute_worker_directory())
                elif job_type == "Index Video":
                    # This is a terminal node.
                    try:
                        index_video(input_data)
                    except Exception as e:
                        logger.error(f"index_video failed {e}")
                        raise e

                    # Do nothing
                    task = mock.mock.delay(input_data, workspace.absolute_worker_directory())
                elif job_type == "Multi Task Learning":
                    logger.info(f"Received Multi Task Learning")
                    try:
                        task = multitask.multitask.delay(input_data, workspace.absolute_worker_directory())
                    except Exception as e:
                        logger.error(f"Multi Task Learning failed {e}")
                        raise e

                else:
                    work_item.status = WorkStatus.ERROR
                    work_item.save()
                    workspace.delete()
                    logger.error(f"Unknown job type: {job_type}")
                    return "ERROR"

                # Wait for worker to finish
                result = AsyncResult(task.id)
                while result.status not in ['SUCCESS', 'FAILURE']:
                    time.sleep(1)

                # Extract the result and save it
                if result.status == "SUCCESS":
                    work_item.status = WorkStatus.DONE
                    if ignore_worker_result:
                        work_item.result = ignore_worker_result
                    else:
                        work_item.result = result.result if isinstance(result.result, dict) else {}
                    
                    # Output data must be dict
                    for nxt_node in self.graph.next_nodes(node):
                        if "input_data" not in nxt_node["data"]:
                            nxt_node["data"]["input_data"] = {}
                        
                        current_input_data = nxt_node["data"]["input_data"]

                        # The input data of the next node is a union
                        nxt_node["data"]["input_data"] = current_input_data | work_item.result
                    
                else:
                    # TODO: If this worker results in an error
                    # how do we prevent the next node from starting?
                    work_item.status = WorkStatus.ERROR
                
                work_item.completed_at = datetime.datetime.now()
                work_item.save()

                print("Node: ", node["data"], "Done") 
            except:
                work_item.status = WorkStatus.ERROR
            finally:
                workspace.release()
                # release all previous workers 
                for prev in self.graph.prev_nodes(node):
                    prev_workspace = prev["workspace"]
                    prev_workspace.release()
    
        thread = Thread(target=worker)
        node["waiter"] = thread
        thread.start()
        return thread
      
    def start(self) -> Thread:
        def main():
            def node_executor(prev_node):

                for node in self.graph.next_nodes(prev_node):
                    self.schedule_workitem(node)
                    node_executor(node)
                
            start_node = self.graph.start_node()
            node_executor(start_node)

        thread = Thread(target=main)
        thread.start()
        return thread


from api.models import Image, Annotation

def handle_ground_truth(input_data: dict):
    logger.debug("handling ground truth")
    output = dict(input_data)
    if input_data.get("bridge", None) is not None:
        bridge_id = input_data['bridge']
        images: list[Image] = list(Image.objects.filter(bridge=int(bridge_id)))
        results = []
        for image in images:
            annotations: list[Annotation] = Annotation.objects.filter(image=image)
            labels = []
            image_data = {
                "image_id": image.id,
                "image_file": image.file.path,
                "labels": labels
            }
            for annot in annotations:
  
                labels.append({
                    "id": annot.id,
                    "region_name": annot.region_name,
                    "region_coord": json.loads(annot.region_coord),
                    "region_share": annot.region_share
                })
            results.append(image_data)
        output["Ground Truth"] = results
    return output

def construct_mask_rcnn_input(input_data:dict):
    if input_data.get("bridge", None):
        bridge_id = input_data['bridge']
        logger.debug(f"Retrieving data for bridge id: {input_data['bridge']}")
        images: list[Image] = list(Image.objects.filter(bridge=int(bridge_id)))
        input_data['images'] = [
            {
                'id': image.id,
                'file': "/media/" + str(image.file),
            } for image in images
        ]
    logger.debug(f"Mask RCNN Input data: {input_data}")
    return input_data

from api.models import FlatVideoIndex
from api.models import Element

def index_video(result: dict):
    video_url = result["video"]
    parts = video_url.split("/media/", 1)

    # Check if split was successful
    if len(parts) == 2:
        # Reconstruct the file path
        file_path = parts[1]
        print("File path:", file_path)
    else:
        print("URL does not contain '/media/' part.")
        return -1
    
    video = Video.objects.get(file=file_path)
    bridge = video.bridge
    index = []
    flat_index: list[FlatVideoIndex] = []

    data = result["result"]

    FlatVideoIndex.objects.filter(video=video).delete()

    logger.info(f"deleted all")

    for frame, info in data.items(): 
        frame_no = info["frame_no"]
        frame_sec = info["frame_sec"]
        labels = []
        for l in info["labels"]:
            labels.append(l["name"])

        for label in set(labels):
            element = Element.objects.filter(id=label).first()
            if element is None:
                element = Element(id=label, name=label, element_number=0)
                element.save()

            flat = FlatVideoIndex(
                bridge=bridge,
                video=video,
                element=element,
                frame_no=frame_no,
                frame_sec=frame_sec,    
            )

            flat_index.append(flat) 

        index.append({
            "frame_no": frame_no,
            "frame_sec": frame_sec,
            "labels": list(set(labels))
        })
    
     
    video_index = VideoIndex(
        video=video,
        index=index
    )

    video_index.save()
    for flat in flat_index:
        flat.save()
    logger.info(f"Index Video Complete - Flat Index length: {len(flat_index)}")
