from django.apps import AppConfig
import time
import threading
class ApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "api"

    def ready(self):
        from .models import Workspace
        from django.utils import timezone

        def worker():
            while True:
                workspaces = Workspace.objects.all()
                for workspace in workspaces:
                    if (
                        workspace.semaphore == 0 
                        and workspace.last_released 
                        and timezone.now() > workspace.last_released + workspace.time_delta
                    ):
                        workspace.delete()

                time.sleep(60)
         
        thread = threading.Thread(target=worker)
        thread.daemon = True
        thread.start()

