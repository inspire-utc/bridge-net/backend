from api.models import Image

def mask_to_vgg(mask_rcnn_output, host="http://localhost:8000"):
    result = mask_rcnn_output["result"]
    vgg = {}
    for image_name, image_result in result.items():
        image_id = image_result["image_id"]
        labels = image_result["labels"]
        image: Image = Image.objects.get(id=image_id)
        regions = []
        vgg[f"{host}/api/images/{image_id}"] = {
            "filename": f"{host}/media/{image.file.name}",
            "regions": regions,
            "file_attributes": {}
        }
        for label in labels:
            name = label["name"]
            polygons = label["polygons"]
            for polygon in polygons:
                regions.append({
                    "id": "",
                    "shape_attributes": {
                        "name": "polygon",
                        "all_points_x": [point[0] for point in polygon],
                        "all_points_y": [point[1] for point in polygon],
                    },
                    "region_attributes": {
                        "Name": name
                    }
                })
    return vgg


def main():
    import json
    import sys
    with open("mask_rcnn_result.json", "r") as fp:
        data = json.load(fp)["workitems"][0]["result"]
    
    with open("vgg.json", "w") as fp:
        json.dump(mask_to_vgg(data), fp, indent=4)

