# Generated by Django 4.2.7 on 2024-02-07 00:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='element',
        ),
        migrations.AddField(
            model_name='image',
            name='element',
            field=models.ManyToManyField(blank=True, related_name='images', to='api.element'),
        ),
    ]
