# Generated by Django 4.2.7 on 2024-02-07 01:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_remove_image_element_image_element'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='element',
        ),
        migrations.AddField(
            model_name='image',
            name='element',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='images', to='api.element'),
        ),
        migrations.CreateModel(
            name='ImageElement',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('element', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.element')),
                ('image', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.image')),
            ],
            options={
                'unique_together': {('image', 'element')},
            },
        ),
    ]
