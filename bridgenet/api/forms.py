""" This file defines forms, which are just html forms. We do not use the actual forms, but they can be used to quickly generate mutations.
They can also be used as a data validator for custom mutation input.
"""

from django import forms
import api.models as Models
from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class UserCreationForm(UserCreationForm):
    class Meta:
        model = Models.User
        fields = ("email", "username")


class UserChangeForm(UserChangeForm):
    class Meta:
        model = Models.User
        fields = ("email", "username")


class CreateBridgeForm(forms.ModelForm):
    class Meta:
        model = Models.Bridge
        fields = ("state_number", "structure_number", "county_name", "year_built")


class CreateElementForm(forms.ModelForm):
    class Meta:
        model = Models.Element
        fields = ("name", "element_number", "parent_element")


class CreateConditionForm(forms.ModelForm):
    class Meta:
        model = Models.Condition
        fields = (
            "type",
            "location",
            "severity",
            "length",
            "units",
            "comment",
            "element",
        )


class CreateImageForm(forms.ModelForm):
    class Meta:
        model = Models.Image
        fields = ("bridge", "element", "condition", "file")
