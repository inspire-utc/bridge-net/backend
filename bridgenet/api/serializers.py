""" This file contains all the serializers for the models. A serializer
is a guide of how to turn some set of data into json. This is mostly automated
by the HyperlinkedModelSerializer class. It will automatically serialize your
model and user hyperlinks to link relationships. The default, ModelSerializer,
uses primary keys. 
"""

from contextlib import nullcontext
from api.models import (
    User, Bridge, Element, Condition, Image, Annotation, Video, Workflow,
    ImageElement
)
from django.core.files.uploadedfile import TemporaryUploadedFile
from rest_framework import serializers
from os import getenv
from time import sleep
from typing import Optional
import logging
import requests
import json

logger = logging.getLogger(__name__)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = [
            "url",
            "id",
            "username",
            "email",
            "groups",
        ]  # only include these fields in response


class ImageMultiUploadSerializer(serializers.HyperlinkedModelSerializer):
    file = serializers.ListField(
        child=serializers.FileField(
            max_length=100000, allow_empty_file=False, use_url=False
        )
    )

    class Meta:
        model = Image
        fields = [
            "url",
            "id",
            "bridge",
            "element",
            "condition",
            "video",
            "frame_number",
            "file",
        ]  # include all fields

    def create(self, validated_data):
        bridge = None
        element = None
        condition = None
        video = None
        frame_number = None
        if "bridge" in validated_data:
            bridge = validated_data["bridge"]
        if "element" in validated_data:
            element = validated_data["element"]
        if "condition" in validated_data:
            condition = validated_data["condition"]
        if "video" in validated_data:
            video = validated_data["video"]
        if "frame_number" in validated_data:
            frame_number = validated_data["frame_number"]
        file = validated_data.pop("file")
        image_list = []
        for img in file:
            photo = Image.objects.create(
                file=img,
                bridge=bridge,
                condition=condition,
                video=video,
                frame_number=frame_number,
            )
            imageurl = f"{photo.file.url}"
            image_list.append(imageurl)
        return image_list


class ImageSerializer(serializers.HyperlinkedModelSerializer):
    # files = serializers.ListField(
    #     child=serializers.ImageField(
    #         max_length=100000, allow_empty_file=False, use_url=False
    #     ),
    #     write_only=True,
    #     allow_null=True,
    #     required=False,
    # )

    class Meta:
        model = Image
        fields = [
            "url",
            "id",
            "bridge",
            "element",
            "condition",
            "video",
            "frame_number",
            "annotations",
            "file",
        ]  # include all fields

    # def create(self, validated_data):
    #     files = None
    #     files = validated_data.pop("files", None)
    #     print(files)
    #     return super().create(validated_data)


# special image serializer for use when in embedded in other serializers.
# The normal image serializer includes all its relationships. That isn't very
# necessary when it is embedded in the data for the model it is related to.
class SimpleImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Image
        fields = ("file", "url", "condition")


class BridgeSerializer(serializers.HyperlinkedModelSerializer):
    elements = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="element-detail"
    )  # inlude related elements by their hyperlinks
    images = SimpleImageSerializer(many=True, read_only=True)
    # show full simple image data (ie hyperlink and viewable image url)
    # many=True says that this should be a list, read_only=True says that you should not
    # be able to include images in a post request (this should probably be changed)
    # making it read_only=False adds some complexity, so you need to program how the
    # writing takes place
    videos = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="video-detail"
    )  # inlude related elements by their hyperlinks

    class Meta:
        model = Bridge
        fields = [
            "url",
            "structure_number",
            "state_number",
            "state_code",
            "county_name",
            "year_built",
            "structure_type",
            "is_ltbp",
            "elements",
            "images",
            "videos",
        ]


class ElementSerializer(serializers.HyperlinkedModelSerializer):
    conditions = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="condition-detail"
    )  # include related condition hyperlinks
    images = SimpleImageSerializer(many=True, read_only=True)
    child_elements = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="element-detail"
    )  # include related child elements hyperlinks

    class Meta:
        model = Element
        fields = [
            "url",
            "id",
            "name",
            "element_number",
            "parent_element",
            "conditions",
            "images",
            "child_elements",
        ]


class ImageElementSerializer(serializers.ModelSerializer):
    # Optionally, include nested serializers for Image and Element to provide more detail
    image = ImageSerializer()
    element = ElementSerializer()
    bridge = BridgeSerializer()

    class Meta:
        model = ImageElement
        fields = ['id', 'image', 'element', 'bridge']  # Include additional fields if needed

class ConditionSerializer(serializers.ModelSerializer):
    bridge_id = serializers.SerializerMethodField()
    images = SimpleImageSerializer(many=True, read_only=True)
    image_file = serializers.ImageField(
        write_only=True, allow_empty_file=False, allow_null=True, required=False
    )

    class Meta:
        model = Condition
        fields = [
            "url",
            "id",
            "type",
            "location",
            "severity",
            "length",
            "units",
            "comment",
            "element",
            "images",
            "image_file",
            "code",
            "condition",
            "environment",
            "defect",
            "image",
            "bridge_id"
        ]
    
    def get_bridge_id(self, obj: Condition) -> int:
        b: Bridge = obj.image.bridge
        return b.structure_number
        

    # This handles uploading a single image file along with the condition,
    # which can be helpful particularly for ARBridge condition uploads.
    def create(self, validated_data):
        imageFile = None

      
        if "image_file" in validated_data:
            imageFile = validated_data.pop("image_file")
        condition = Condition.objects.create(**validated_data)
        logger.info(condition.defect)
        if imageFile:
            Image.objects.create(condition=condition, file=imageFile) 
        return condition  

 
class AnnotationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Annotation
        fields = ["url", "id", "region_name", "region_coord", "region_share", "image"]


class VideoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Video
        fields = ["url", "bridge", "images", "important_frames", "file"]


class VideoMultiUploadSerializer(serializers.HyperlinkedModelSerializer):
    file = serializers.ListField(
        child=serializers.FileField(
            max_length=100000, allow_empty_file=False, use_url=False
        )
    )

    class Meta:
        model = Video
        fields = [
            "url",
            "id",
            "bridge",
            "images",
            "file",
            "important_frames"
        ]  # include some fields

    def create(self, validated_data):
        bridge = None
        files: list[TemporaryUploadedFile] = validated_data.pop("file")
        frame_nums: Optional[list[int]] = []
        if "bridge" in validated_data:
            # frame_nums = []
            bridge = validated_data["bridge"]
            # multi-line commented code below goes here
        video_list = []
        for video in files:
            video_obj = Video.objects.create(
                file=video, bridge=bridge, important_frames=frame_nums
            )
            videourl = f"{video_obj.file.url}"
            video_list.append((videourl, video_obj.id))
        return video_list

"""framediff_url: str = str(getenv("FRAMEDIFF_URL"))

            files[0].seek(0)
            file_bytes = files[0].file.read()
            framediff_res: requests.Response = requests.post(f"{framediff_url}/upload", files={"file": (files[0].name, file_bytes, "video/mp4")})
            framediff_res.raise_for_status()
            framediff_job_id: str = framediff_res.json()["job_id"]

            with requests.Session() as s_status:  # session status
                framediff_status: requests.Response = s_status.get(
                    f"{framediff_url}/status/{framediff_job_id}"
                )
                framediff_status.raise_for_status()
                while not framediff_status.json()["selected_frames"]:
                    print(framediff_status.json())
                    sleep(1)
                    framediff_status = s_status.get(
                        f"{framediff_url}/status/{framediff_job_id}"
                    )
                    framediff_status.raise_for_status()

            for selected_frame in framediff_status.json()["selected_frames"]:
                frame_nums.append(selected_frame["frame_no"])

            frame_nums.sort()

        files[0].seek(0)"""


class WorkflowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workflow
        fields = '__all__'
    
from .models import WorkItem

class WorkItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkItem
        fields = '__all__'
    
    


from .models import WorkflowJob
from .workflow_interpreter import WorkflowInterpreter

class WorkflowJobSerializer(serializers.ModelSerializer):
    workitems = WorkItemSerializer(many=True, read_only=True, source='workitem_set')
    associated_workflow = serializers.UUIDField(write_only=True)  # Field to accept the associated_workflowid in the POST body
    nodes = serializers.JSONField(required=False)
    edges = serializers.JSONField(required=False)


    class Meta:
        model = WorkflowJob
        fields = ["id", "name", "date_created", "date_modified", "associated_workflow", "workitems", "nodes", "edges"]

    def create(self, validated_data):
        # Extract associated_workflow from validated_data
        associated_workflow_id = validated_data.pop('associated_workflow', None)

        if associated_workflow_id is not None:
            # Get the associated Workflow instance
            associated_workflow = Workflow.objects.get(id=associated_workflow_id)
            
            # Copy nodes and edges from associated_workflow
            validated_data['nodes'] = associated_workflow.nodes
            validated_data['edges'] = associated_workflow.edges
            validated_data['name'] = associated_workflow.name

         
        # Create the WorkflowJob instance
        workflow_job_instance = WorkflowJob.objects.create(**validated_data)

        interpreter = WorkflowInterpreter(workflow_job_instance)
        interpreter.start()
        
        return workflow_job_instance


from .models import VideoIndex  # Adjust import path as per your project structure

class VideoIndexSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoIndex
        fields = ['id', 'video', 'index']


from .models import FlatVideoIndex

class FlatVideoIndexSerialzer(serializers.ModelSerializer):
    class Meta:
        model = FlatVideoIndex
        fields = [ 'id', 'video', 'bridge', 'element', 'frame_no', 'frame_sec' ]
