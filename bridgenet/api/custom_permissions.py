""" Define any custom permissions needed by the api here.
"""

from rest_framework import permissions


class IsCurrentUserUser(permissions.BasePermission):
    """Permission that only allows a user to view a user's information
    if they are the user being requested or a superuser"""

    # checks the specific object
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return obj == request.user or request.user.is_superuser
        else:
            return False
