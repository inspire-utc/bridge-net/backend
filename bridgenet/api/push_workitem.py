from celery.result import AsyncResult
from .models import WorkItem, WorkStatus
import threading
import time
import datetime


# Import workers
from api.workers.framediff import framediff
from api.workers.split_video import split_video


def register_callback(task):
    def wrapper(callback):
        result = AsyncResult(task.id)
        def waiter():
            while result.status not in ['SUCCESS', 'FAILURE']:
                time.sleep(1)
            callback(result)

        thread = threading.Thread(target=waiter)
        thread.start()
        return thread
    return wrapper


def push_workitem(job_id, job_type, input_data):
    
    work_item = WorkItem.objects.create(
        type=job_type,
        input_data=input_data,
        started_at=datetime.datetime.now()
    )
   
    
    if job_type == "framediff":
        task = framediff.framediff.delay(input_data)
    elif job_type == "Split Video":
        task = split_video.split_video.delay(input_data)
    elif job_type == "Mask RCNN Inference":
        pass


    @register_callback(task)
    def handle_task_complete(result:AsyncResult):
        print("DONE")
        record = WorkItem.objects.get(id=work_item.id)
        if result.status == "SUCCESS":
            record.status = WorkStatus.DONE
            record.result = result.result
        else:
            record.status = WorkStatus.ERROR
        
        record.completed_at = datetime.datetime.now()
        record.save()