from typing import *

class VideoIndex_ElementIndexField(TypedDict):
    frame_no: int
    element_name: str
    duration_sec: float

class VideoIndexData(TypedDict):
    elements: List[VideoIndex_ElementIndexField]
    
