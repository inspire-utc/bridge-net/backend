""" This file contains all the views for the website. A view is a page on the website.
For our use case with REST, a view tends to be a single object to request or a single
type of request.
"""
import cv2
import requests
import json
import numpy as np
import logging
from typing import Any

from api.serializers import (
    ImageMultiUploadSerializer,
    UserSerializer,
    BridgeSerializer,
    ElementSerializer,
    ConditionSerializer,
    ImageSerializer,
    AnnotationSerializer,
    VideoSerializer,
    VideoMultiUploadSerializer,
    ImageElementSerializer
)
from rest_framework import viewsets, permissions, status, views
from rest_framework.parsers import JSONParser
from api.models import (
    User,
    Bridge,
    Element,
    Condition,
    Image,
    MultiToken,
    Annotation,
    Video,
    ImageElement
)

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework.request import Request
from api import custom_permissions
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from rest_framework import pagination

logger = logging.getLogger(__name__)

# view uses the UserSerializer to return a user object
class UserView(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()  # How to query users
    filterset_fields = "__all__"
    ordering = ["id"]

    # overriding default retrieve to explicitly check object permissions
    def retrieve(self, request, pk):
        user = get_object_or_404(User, pk=pk)
        self.check_object_permissions(
            request, user
        )  # need to explicitly check object permissions
        serializer = UserSerializer(user, context={"request": request})
        return Response(serializer.data)

    def get_permissions(self):
        # for listing and creating users
        if self.action == "list" or self.action == "create":
            permission_classes = [
                permissions.IsAdminUser,
            ]
        # for any other case
        else:
            permission_classes = [custom_permissions.IsCurrentUserUser]

        return [permission() for permission in permission_classes]


class BridgeView(viewsets.ModelViewSet):
    """A 10-elements-per-page paginated list of all bridges in the database.

    ### Variables:
    - **url** *(Hyperlink - R)*: The url of this bridge.
    - **elements** *(Hyperlink[] - R)*: List of links to all child elements of this bridge.
    - **images** *(ImageSlug[] - R)*: List of all images associated with this bridge.
        - **url** *(Hyperlink - R)*: The url of this image in the api.
        - **file** *(Hyperlink - R)*: The image url. Ie url pointing directly to the viewable image file.
    - **state_number** *(Int - RW)*: Federally administered number for state bridge resides in.
    - **structure_numer** *(Int - RW)*: Unique bridge identifying structure number.
    - **county_name** *(String - RW)*: County bridge resides in.
    - **year_built** *(Int - RW)*: Year bridge was built.
    - **structure_type** *(String - RW)*: Bridge Girder Type
    - **is_ltbp** *(Boolean - RW)*: True if bridge is Long-Term Bridge Performance
    """

    # This docstring actually acts as the api documentation, so do not edit it carelessly

    serializer_class = BridgeSerializer
    queryset = Bridge.objects.all()
    filterset_fields = {
        "state_number": ["exact", "gt", "lt", "gte", "lte", "isnull"],
        "structure_number": ["exact", "gt", "lt", "gte", "lte", "isnull"],
        "county_name": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "year_built": ["exact", "gt", "lt", "gte", "lte", "isnull"],
        "structure_type": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "state_code": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "is_ltbp": ["exact"],
    }
    ordering = ["structure_number"]


class ElementView(viewsets.ModelViewSet):
    """A 10-elements-per-page paginated list of all elements in the database.

    ### Variables:
    - **url** *(Hyperlink - R)*: The url of this element.
    - **conditions** *(Hyperlink[] - R)*: List of links to all child conditions of this element.
    - **images** *(ImageSlug[] - R)*: List of all images associated with this bridge.
        - **url** *(Hyperlink - R)*: The url of this image in the api.
        - **file** *(Hyperlink - R)*: The image url. Ie url pointing directly to the viewable image file.
    - **child_elements** *(Hyperlink[] - R)*: List of links to all child elements of this element.
    - **name** *(String - RWN)*: Typical name of element.
    - **element_number** *(Int - RW)*: Element number based on type according to FHWA and AASHTO.
    - **bridge** *(Hyperlink - RW)*: Parent bridge of this element.
    - **parent_element** *(Hyperlink - RWN)*: Potential parent element of this element.
    """

    serializer_class = ElementSerializer
    queryset = Element.objects.all()
    filterset_fields = {
        "id": ["exact", "gt", "lt", "gte", "lte", "isnull"],
        "name": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "element_number": ["exact", "gt", "lt", "gte", "lte", "isnull"],
        "parent_element": ["exact", "isnull"],
    }
    ordering = ["id"]


class ImageElementView(viewsets.ModelViewSet):
    serializer_class = ImageElementSerializer
    queryset = ImageElement.objects.all()
    filterset_fields = {
        "image": ["exact", "isnull"],
        "element": ["exact", "isnull"],
        "bridge": ["exact", "isnull"],
    }
    


class ConditionView(viewsets.ModelViewSet):
    """A 10-elements-per-page paginated list of all conditions in the database.

    ### Variables:
    - **url** *(Hyperlink - R)*: The url of this condition.
    - **images** *(ImageSlug[] - R)*: List of all images associated with this condition.
        - **url** *(Hyperlink - R)*: The url of this image in the api.
        - **file** *(Hyperlink - R)*: The image url. Ie url pointing directly to the viewable image file.
    - **type** *(String - RW)*: What type of condition it is. Ie cracks.
    - **location** *(String - RW)*: What the location of the condition is on the element.
    - **severity** *(String - RW)*: How severe the condition is.
    - **length** *(String - RW)*: Length of the condition on the element.
    - **units** *(String - RW)*: Units for the length of the condition.
    - **comment** *(String - RWN)*: Any extra comments on the condition.
    - **element** *(Hyperlink - RW)*: Parent element for this condition.
    - **image_file** *(File - WN)*: File of image associated with condition to upload.
    """

    serializer_class = ConditionSerializer
    queryset = Condition.objects.all()
    filterset_fields = {
        "id": ["exact", "gt", "lt", "gte", "lte", "isnull"],
        "type": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "location": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "severity": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "length": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "units": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "comment": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "element": ["exact", "isnull"],
        "image": ["exact", "isnull"]
    }
    ordering = ["id"]

    def put(self, *args, **kwargs):
        return self.update(*args, **kwargs) 
  
class ImageUploadView(viewsets.ModelViewSet):
    filterset_fields = {
        "bridge": ["exact", "isnull"],
        "element": ["exact", "isnull"],
        "condition": ["exact", "isnull"],
    }
    serializer_class = ImageSerializer
    queryset = Image.objects.all()
    ordering = ["id"]

    def create(self, request):
        serializer = ImageMultiUploadSerializer(data=request.data)
        if serializer.is_valid():  # validate the serialized data to make sure its valid
            qs = serializer.save()
            message = {"detail": qs, "status": True}
            return Response(message, status=status.HTTP_201_CREATED)
        else:  # if the serialzed data is not valid, return error response
            data = {"detail": serializer.errors, "status": False}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        return Image.objects.all()


class ImageView(viewsets.ModelViewSet):
    """A 10-elements-per-page paginated list of all images in the database.
    TODO: Need to modify to support uploading multiple files at once https://roytuts.com/single-and-multiple-files-upload-example-in-django-rest-api/

    ### Variables:
    - **url** *(Hyperlink - R)*: The url of this image.
    - **bridge** *(Hyperlink - RWN)*: Parent bridge for this image.
    - **element** *(Hyperlink - RWN)*: Parent element for this image.
    - **condition** *(Hyperlink - RWN)*: Parent condition for this image.
    """

    serializer_class = ImageSerializer
    queryset = Image.objects.all()
    filterset_fields = {
        "bridge": ["exact", "isnull"],
        "element": ["exact", "isnull"],
        "condition": ["exact", "isnull"],
    }
    ordering = ["id"]


# This view is used for authenticating
# It creates a token every time
# TODO implement a TestLogin view
# that allows users to check if they are logged in
# to avoid re-logging in.
class LoginApi(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        context = dict(request=request, view=self)
        serializer = self.serializer_class(
            data=request.data, context=context
        )  # use context to serialize data
        serializer.is_valid(raise_exception=True)  # check if valid
        user = serializer.validated_data[
            "user"
        ]  # if valid data (ie valid username and password)
        token = MultiToken.objects.create(
            user=user
        )  # create a token object assigned to this user
        data = {"token": token.key, "user": user.id}  # response to give back to user

        return Response(data)  # send response with token


class AnnotationView(viewsets.ModelViewSet):
    """A 10-elements-per-page paginated list of all bridges in the database.

    ### Variables:
    - **url** *(Hyperlink - R)*: The url of this annotation.
    - **region_name** *(String - RW)*: Annotation region name
    - **region_coord** *(String - RW)*: Annotation coordinates
    - **region_share** *(String - RW)*: Annotation share???
    - **image** *(Image - R)*: Image this annotation belongs to
    """

    # This docstring actually acts as the api documentation, so do not edit it carelessly

    serializer_class = AnnotationSerializer
    queryset = Annotation.objects.all()
    filterset_fields = {
        "region_name": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "region_coord": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "region_share": ["exact", "startswith", "endswith", "icontains", "isnull"],
        "image": ["exact", "isnull"],
    }
    ordering = ["id"]


class VideoUploadView(viewsets.ModelViewSet):
    filterset_fields = {
        "bridge": ["exact", "isnull"],
        "id": [ "exact" ]
    }
    serializer_class = VideoSerializer
    queryset = Video.objects.all()
    ordering = ["id"]

    def create(self, request):
        serializer = VideoMultiUploadSerializer(data=request.data)
        if serializer.is_valid():  # validate the serialized data to make sure its valid
            qs: list[tuple(str, str)] = serializer.save()
            message = {"detail": qs, "status": True}
            return Response(message, status=status.HTTP_201_CREATED)
        else:  # if the serialzed data is not valid, return error response
            data = {"detail": serializer.errors, "status": False}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        return Video.objects.all()


def write_video(data: str) -> None:
    """
    This function writes a video frame by frame to the
    corresponding bridge by self calling the django
    images route over and over again.
    """
    video_path: str = data[0]
    vid_id: str = data[1]

    bridge_num: str = video_path.split("/")[3]
    if not bridge_num.isnumeric():
        return  # means that video was not uploaded to a specific bridge folder

    video_path = "." + video_path

    cap: cv2.VideoCapture = cv2.VideoCapture(video_path)

    count: int = 0
    while True:
        ok, frame = cap.read()
        if not ok:
            break

        ok, frame_encode = cv2.imencode(".jpg", frame)
        if not ok:
            continue  # best thing to do here?????

        file_tuple: tuple[str, bytes, str] = (
            f"frame_{count}.jpg",
            frame_encode.tobytes(),
            "image/jpeg",
        )

        files: dict[str, tuple[str, bytes, str]] = {"file": file_tuple}
        headers: dict[str, str] = {"Authorization": f"Bearer test_oauth_token"}
        data: dict[str, str] = {
            "bridge": f"http://localhost:8000/api/bridges/{bridge_num}/",
            "frame_number": count,
            "video": f"http://localhost:8000/api/videos/{vid_id}/",
        }

        r: requests.Response = requests.post(
            "http://localhost:8000/api/images/", data=data, headers=headers, files=files
        )
        r_status = r.status_code

        count += 1

    cap.release()

    return


class PropogateView(viewsets.ModelViewSet):
    def create(self, request, format=None):
        if request.method == "POST":
            annotations: dict[str, Any] = json.loads(json.loads(request.body)[0])
            first_key: str = list(annotations.keys())[0]
            bridge_num: int = int(first_key.split("/")[5])
            img_name: str = first_key.split("/")[6]
            
            img = cv2.imread(f"./media/images/{bridge_num}/{img_name.replace('-1','')}")
            img_shape: tuple[int, int] = img.shape[0:2]
            del img
            blank_img = np.zeros(shape=img_shape, dtype=np.uint8)

            imgs = []

            for img_url, other_attrs in annotations.items():
                if other_attrs["regions"]:  # if there is something in the regions list
                    for region in other_attrs["regions"]:
                        x_pts = region["shape_attributes"]["all_points_x"]
                        y_pts = region["shape_attributes"]["all_points_y"]
                        coords: list[list[int, int]] = np.array([[x, y] for x, y in zip(x_pts, y_pts)])
                        cv2.fillPoly(blank_img, pts=[coords], color=255)  # 255 writes white in a picture
                        imgs.append(blank_img)

                blank_img = np.zeros(shape=img_shape, dtype=np.uint8)

            imgs.append(blank_img)  # append last image
            imgs = np.array(imgs).transpose((1,2,0))

            cv2.imwrite("frame_0_bw.jpg", imgs[:,:, 0])

            return Response({"status": True}, status=status.HTTP_201_CREATED)
        else:
            return Response({"status": True}, status=status.HTTP_400_BAD_REQUEST)

        
from django.views.decorators.clickjacking import xframe_options_exempt


@xframe_options_exempt  # Allow iframes
def Annotator(request, bridge: int = -1):
    """
    This view constructs the VIA Annotator
    """
    import os
    import json
    import requests

    from urllib.parse import urlparse

    # Do a hack to validate user login status
    token = request.GET.get("token", "")

    bridge = request.GET.get("structure_number", "")

    # Preview one token at a time
    preview = request.GET.get("preview", "")

    # Read the templates
    with open("annotator-template/via-index.html.template") as fp:
        index_template = fp.read()
    with open("annotator-template/via.js.template") as fp:
        js_template = fp.read()
    with open("annotator-template/via.css") as fp:
        style = fp.read()


    def construct_polygon_polyline_orm(annot):
        """
        Construct Annotated region from ORM Object
        """

        region_coord = json.loads(annot.region_coord)
        all_x = [x[0] for x in region_coord]
        all_y = [x[1] for x in region_coord]

        return {
            "id": annot.id,
            "region_attributes": {"Name": annot.region_name},
            "shape_attributes": {
                "name": annot.region_share,
                "all_points_x": all_x,
                "all_points_y": all_y,
            },
        }

    # Do a self-referential get request to extract all images to inject into annotator
    image_key: str = "results"  # the file and url are stored in different names in api/images and api/bridges
    if bridge == -1:
        req = requests.get("http://localhost:8000/api/images")
    else:
        req = requests.get(f"http://localhost:8000/api/bridges/{bridge}")
        image_key = "images"

    req_body = req.json()

 
    images = req_body[image_key]
    if preview:
        images = [img for img in images if img["file"] == preview]
    injected_obj = {}

    for image in images:
        img_path = image["file"]  # Where the image resource is located
        img_url = image["url"]  # Where the api resource is located
        regions = []
        if "annotations" not in image:
            r_json: requests.Response = requests.get(img_url).json()
            image["annotations"] = r_json["annotations"]
        for annotation_url in image["annotations"]:
            # Extract annotation id from path...
            url_path = urlparse(annotation_url).path
            frag = url_path.split("/")
            id = int(frag[-2])

            # ... so we can query the ORM
            annot = Annotation.objects.get(id=id)

            # req = requests.get(annotation_url)
            # annot = req.json()

            # Ignore other shapes because the region data is different
            if annot.region_share != "polygon" and annot.region_share != "polyline":
                continue

            try:
                region = construct_polygon_polyline_orm(annot)
            except:
                continue
            else:
                regions.append(region)

        injected_obj[img_url] = {
            "filename": img_path,
            "size": -1,
            "regions": regions,
            "file_attributes": {},
        }

    # Inject the annotation objects into annotator script
    js_source = js_template.replace("%JSON_STRING%", json.dumps(injected_obj)).replace(
        "%TOKEN%", token
    )

    # Inject the annotator script
    html = (
        index_template.replace("%SOURCE%", js_source)
        .replace("%STYLE%", style)
        .replace("%API_ROOT%", "http://localhost:8000")
    )
    return HttpResponse(html)


def AnnotatedImage(req, image=0):
    import os
    import requests
    import base64
    import cv2
    import json
    import numpy as np
    import random
    from urllib.parse import urlparse
    from io import BytesIO

    image_orm = Image.objects.get(id=image)

    # Do a self-referential get request to extract all images to inject into annotator
    req = requests.get(f"http://localhost:8000/api/images/{image}")
    req_body = req.json()
    file = req_body["file"]
    annotations = req_body["annotations"]

    file_path = "media/" + str(image_orm.file)

    img = cv2.imread(file_path)

    def random_color():
        rand = lambda: random.randint(0, 255)
        return (rand(), rand(), rand())

    for annotation_url in annotations:
        url_path = urlparse(annotation_url).path
        frag = url_path.split("/")
        id = int(frag[-2])

        # ... so we can query the ORM
        annot = Annotation.objects.get(id=id)
        points = np.array(json.loads(annot.region_coord))
        try:
            cv2.polylines(img, np.int32([points]), True, random_color(), 10)
        except:
            continue
    is_success, buffer = cv2.imencode(".jpg", img)
    io_buf = BytesIO(buffer)
    return HttpResponse(io_buf.getvalue(), content_type="image/png")

## WORKFLOW

from .models import Workflow
from .serializers import WorkflowSerializer

class WorkflowView(viewsets.ModelViewSet):
    queryset = Workflow.objects.all()
    serializer_class = WorkflowSerializer

    def put(self, request, *args, **kwargs):
        print(request.data)

        workflow = Workflow.objects.get(id=request.data["id"])
        workflow.nodes = request.data["nodes"]
        workflow.edges = request.data["edges"]
        workflow.name = request.data["name"]
        workflow.date_modified = datetime.datetime.now()
        workflow.save()
        
        return Response({"status": "OK"}, status=status.HTTP_200_OK)
 
## WORKITEM
from .models import WorkItem, WorkStatus
from .serializers import WorkItemSerializer

class WorkItemView(viewsets.ModelViewSet):
    queryset = WorkItem.objects.all()
    serializer_class = WorkItemSerializer


from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .dto import WorkItemRequestDTO
from celery.result import AsyncResult
import threading
import time
import datetime
from .push_workitem import push_workitem

def register_callback(task):
    def wrapper(callback):
        result = AsyncResult(task.id)
        def waiter():
            while result.status not in ['SUCCESS', 'FAILURE']:
                time.sleep(1)
            callback(result)
        threading.Thread(target=waiter).start()
    return wrapper


@api_view(['POST'])
def create_workitem(request):
    if request.method == 'POST':
        serializer = WorkItemRequestDTO(data=request.data)
        if serializer.is_valid():
            # Here you can add logic to process the computational job
            # For now, we'll just return the validated data
            
            job_id = serializer.validated_data.get('job_id')
            job_type = serializer.validated_data.get('type')
            input_data = serializer.validated_data.get('input_data')
            push_workitem(job_id, job_type, input_data)
            return Response(serializer.validated_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def JobView(request):
    return HttpResponse("Hello")


from .models import WorkflowJob
from .serializers import WorkflowJobSerializer

class WorkflowJobViewSet(viewsets.ModelViewSet):
    queryset = WorkflowJob.objects.all()
    serializer_class = WorkflowJobSerializer

    class pagination_class(pagination.PageNumberPagination):
        page_size = 10
        page_size_query_param = 'page_size'
        max_page_size = 100

    ordering = ['-date_created']
    

def render_mask_rcnn_inference(request, workitem:WorkItem):
    def mask_to_vgg(mask_rcnn_output, host="http://localhost:8000"):
        result = mask_rcnn_output["result"]
        vgg = {}
        for image_name, image_result in result.items():
            image_id = image_result["image_id"]
            image_url = image_result["image_url"]
            labels = image_result["labels"]
            
            regions = []
            if image_result.get("database", False):
                image: Image = Image.objects.get(id=image_id)
            
                
                vgg[f"{host}/api/images/{image_id}/"] = {
                    "filename": f"{host}/media/{image.file.name}",
                    "regions": regions,
                    "file_attributes": {}
                }
            else:
                vgg[image_url] = {
                    "filename": image_url,
                    "regions": regions,
                    "file_attributes": {}
                }

            for label in labels:
                name = label["name"]
                polygons = label["polygons"]
                for polygon in polygons:
                    regions.append({
                        "id": "",
                        "shape_attributes": {
                            "name": "polygon",
                            "all_points_x": [point[0] for point in polygon],
                            "all_points_y": [point[1] for point in polygon],
                        },
                        "region_attributes": {
                            "Name": name
                        }
                    })
        return vgg

    mask_rcnn_output = workitem.result
    vgg = mask_to_vgg(mask_rcnn_output)
    # Do a hack to validate user login status
    token = request.GET.get("token", "")

    logger.debug(f"token: {token}")

    # Read the templates
    with open("annotator-template/via-index.html.template") as fp:
        index_template = fp.read()
    with open("annotator-template/via.js.template") as fp:
        js_template = fp.read()
    with open("annotator-template/via.css") as fp:
        style = fp.read()
    
    # Inject the annotation objects into annotator script
    js_source = js_template.replace("%JSON_STRING%", json.dumps(vgg)).replace(
        "%TOKEN%", token
    )

    # Inject the annotator script
    html = (
        index_template
        .replace("%SOURCE%", js_source)
        .replace("%STYLE%", style)
        .replace("%API_ROOT%", "http://localhost:8000")
    )
    return HttpResponse(html)

@api_view(['GET'])
@xframe_options_exempt  # Allow iframes
def workitem_render(request, workitem_id):
    workitem = WorkItem.objects.get(id=workitem_id)

    if workitem.type == "Mask RCNN Inference":
        return render_mask_rcnn_inference(request, workitem)
    elif workitem.type == "Ground Truth":
        return  HttpResponse(f"<div>{json.dumps(workitem.result)}</div>")
    elif workitem.type == "Compute Metric":
        return render_compute_metric(request, workitem)
    elif workitem.type == "Multi Task Learning":
        return render_multitask_learning(request, workitem)
    else:
        return Response({"detail": "Invalid workitem type"}, status=status.HTTP_400_BAD_REQUEST)

import hashlib



import json
from django.http import HttpResponse
from django.http import HttpRequest
def render_multitask_learning(request:HttpRequest, workitem):
    result = workitem.result
    segmentation_images = result["element_segmentation"]
    damage_images = result["damage_segmentation"]
    element_vgg = result["element_vgg"]
    damage_vgg = result["damage_vgg"]

    def render_vgg_annotator(vgg):
        # Do a hack to validate user login status
        token = request.GET.get("token", "")

        logger.debug(f"token: {token}")

        # Read the templates
        with open("annotator-template/via-index.html.template") as fp:
            index_template = fp.read()
        with open("annotator-template/via.js.template") as fp:
            js_template = fp.read()
        with open("annotator-template/via.css") as fp:
            style = fp.read()
        
        # Inject the annotation objects into annotator script
        js_source = js_template.replace("%JSON_STRING%", json.dumps(vgg)).replace(
            "%TOKEN%", token
        )

        # Inject the annotator script
        html = (
            index_template
            .replace("%SOURCE%", js_source)
            .replace("%STYLE%", style)
            .replace("%API_ROOT%", "/")
        )
        return HttpResponse(html)

    # check if ?request=element or ?request=damage
    request_type = request.GET.get("request")
    if request_type is None:
        # Ensure both lists have the same length
        paired_images = zip(segmentation_images, damage_images)

        images_html = ""

        for seg_image, dmg_image in paired_images:
            seg_image_path = os.path.join("/app/bridgenet", seg_image)
            dmg_image_path = os.path.join("/app/bridgenet", dmg_image)

            images_html += f"""
            <div class="image-row">
                <div class="image-container">
                    <img src='{seg_image_path}' class="image"/>
                </div>
                <div class="image-container">
                    <img src='{dmg_image_path}' class="image"/>
                </div>
            </div>
            """

        html_content = f"""
        <html>
        <head>
            <style>
                body {{
                    font-family: Arial, sans-serif;
                    background-color: #f4f4f4;
                    text-align: center;
                    margin: 0;
                }}
                .sticky-header {{
                    position: sticky;
                    top: 0;
                    background: white;
                    padding: 15px 0;
                    font-size: 22px;
                    font-weight: bold;
                    color: #003366;
                    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
                    z-index: 1000;
                    display: flex;
                    justify-content: space-around;
                }}
                .container {{
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                    padding: 20px;
                }}
                .image-row {{
                    display: flex;
                    justify-content: center;
                    gap: 30px;
                    margin-bottom: 20px;
                    width: 80%;
                }}
                .image-container {{
                    width: 45%;
                }}
                .image {{
                    max-width: 100%;
                    border-radius: 10px;
                }}
            </style>
        </head>
        <body>                    
            <div class="sticky-header">
                <div><a href='/render/workitems/{workitem.id}?request=element'>Element Segmentation</a></div>
                <div><a href='/render/workitems/{workitem.id}?request=damage'>Damage Segmentation</a></div>
            </div>
            <div class="container">
                {images_html}
            </div>
        </body>
        </html>
        """
        return HttpResponse(html_content)

    elif request_type == "element":
        return render_vgg_annotator(element_vgg)
    elif request_type == "damage":
        return render_vgg_annotator(damage_vgg)

def render_compute_metric(request, workitem):
    compute_metric = workitem.result["Compute Metric"]
    metrics = compute_metric["Metrics"]
    
    # Initialize dictionaries for summary and detailed metrics
    global_metric_sums = {}
    global_metric_counts = {}
    image_metric_sums = {}
    image_metric_counts = {}
    detailed_metrics = {}

    # Iterate over each image's metrics
    for image_metric in metrics:
        image_id = image_metric["image_id"]
        label_metrics = image_metric["label_metrics"]
        
        # For each label in the image
        for label, metric_values in label_metrics.items():
            # Initialize metrics for summary if not already
            if label not in global_metric_sums:
                global_metric_sums[label] = {'iou': 0, 'f1': 0, 'prec': 0, 'recall': 0}
                global_metric_counts[label] = {'iou': 0, 'f1': 0, 'prec': 0, 'recall': 0}
            
            # Initialize metrics for each label and image if not already
            if image_id not in image_metric_sums:
                image_metric_sums[image_id] = {}
                image_metric_counts[image_id] = {}
            if label not in image_metric_sums[image_id]:
                image_metric_sums[image_id][label] = {'iou': 0, 'f1': 0, 'prec': 0, 'recall': 0}
                image_metric_counts[image_id][label] = {'iou': 0, 'f1': 0, 'prec': 0, 'recall': 0}
            
            # Collect and sum metrics per image and label
            for metric in ['iou', 'f1', 'prec', 'recall']:
                values = metric_values.get(metric, [])
                for value in values:
                    if value is not None:
                        # Sum for image specific metrics
                        image_metric_sums[image_id][label][metric] += value
                        image_metric_counts[image_id][label][metric] += 1
                        # Sum for global metrics
                        global_metric_sums[label][metric] += value
                        global_metric_counts[label][metric] += 1

    # Calculate averages for each image and label
    for image_id in image_metric_sums:
        detailed_metrics[image_id] = {}
        for label in image_metric_sums[image_id]:
            detailed_metrics[image_id][label] = {}
            for metric in image_metric_sums[image_id][label]:
                if image_metric_counts[image_id][label][metric] > 0:
                    average = image_metric_sums[image_id][label][metric] / image_metric_counts[image_id][label][metric]
                else:
                    average = 0
                detailed_metrics[image_id][label][metric] = average

    # Calculate global averages for summary metrics
    avg_metrics = {
        label: {
            metric: (global_metric_sums[label][metric] / global_metric_counts[label][metric] if global_metric_counts[label][metric] > 0 else 0)
            for metric in global_metric_sums[label]
        }
        for label in global_metric_sums
    }

    # HTML for Summary Table
    html_summary = '<table border="1" style="width:100%; border-collapse: collapse;">'
    html_summary += ('<tr>'
                     '<th>Label</th>'
                     '<th>Average IoU</th>'
                     '<th>Average F1</th>'
                     '<th>Average Precision</th>'
                     '<th>Average Recall</th>'
                     '</tr>')
    for label, metrics in avg_metrics.items():
        html_summary += (f'<tr>'
                         f'<td>{label.capitalize()}</td>'
                         f'<td>{metrics["iou"]:.3f}</td>'
                         f'<td>{metrics["f1"]:.3f}</td>'
                         f'<td>{metrics["prec"]:.3f}</td>'
                         f'<td>{metrics["recall"]:.3f}</td>'
                         f'</tr>')
    html_summary += '</table>'

    # HTML for Detailed Table by Image
    html_detail = '<table border="1" style="width:100%; border-collapse: collapse;">'
    html_detail += ('<tr>'
                    '<th>Image ID</th>'
                    '<th>Label</th>'
                    '<th>Average IoU</th>'
                    '<th>Average F1</th>'
                    '<th>Average Precision</th>'
                    '<th>Average Recall</th>'
                    '</tr>')
    for image_id, labels in detailed_metrics.items():
        for label, metrics in labels.items():
            html_detail += (f'<tr>'
                            f'<td>{image_id}</td>'
                            f'<td>{label.capitalize()}</td>'
                            f'<td>{metrics["iou"]:.3f}</td>'
                            f'<td>{metrics["f1"]:.3f}</td>'
                            f'<td>{metrics["prec"]:.3f}</td>'
                            f'<td>{metrics["recall"]:.3f}</td>'
                            f'</tr>')
    html_detail += '</table>'

    # Combine both tables in the response
    html = f"<h2>Summary Metrics Across All Images</h2>{html_summary}<h2>Detailed Average Metrics by Image</h2>{html_detail}"
    return HttpResponse(f"<div>{html}</div>")

# This function would still be called with a WorkItem instance as before.


# This function would still be called with a WorkItem instance as before.

# Example usage would be as previously described, passing in a WorkItem instance.

# This function would be called with a WorkItem instance similar to previous examples.
# Example usage:
# response = render_compute_metric(request, workitem)
# This will return the response containing the HTML table with averaged metrics.

# Assuming a WorkItem class or a similar data structure, you would pass this to the function.
# For example:
# workitem = WorkItem(result={"Compute Metric": {"Model": None, "Metrics": [...]}})
# render_compute_metric(request, workitem)


def stable_hash_tuple(input_tuple):
    # Convert the tuple to a string representation. Ensure it's in a consistent order if it contains any unordered collections.
    tuple_str = str(input_tuple)
    
    # Use SHA-256 hashing algorithm to generate a hash of the string representation.
    hash_object = hashlib.sha256(tuple_str.encode())
    
    # Return the hexadecimal representation of the hash.
    return int(str(hash_object.hexdigest())[:16], base=16)

@api_view(["POST", "PUT", "DELETE"])
def update_elements(request, annotation_id):
    annotation_id = int(annotation_id)
    annot:Annotation = Annotation.objects.get(id=annotation_id)
    image = annot.image
    bridge = annot.image.bridge
    # we need stable hash because python hash() changes values when the interpreter restarts
    element, created = Element.objects.get_or_create(
        id=annot.region_name,
        name=annot.region_name,
        element_number=0
    )
    assert element

    image_element, created = ImageElement.objects.get_or_create(
        bridge=bridge,
        image=image,
        element=element,
    )
    image_element.save()

    annot.element = element

    annot.save() 

    if created:
        logger.debug(f"created: {element.id}")
    else:
        logger.debug(f"Not created")
    
    return Response({"detail": "OK"}, status=status.HTTP_200_OK)


from .models import VideoIndex
from .serializers import VideoIndexSerializer

class VideoIndexListCreate(viewsets.ModelViewSet):
    serializer_class = VideoIndexSerializer
    def get_queryset(self):
        video_id = self.request.query_params.get('video_id')
        if video_id:
            return VideoIndex.objects.filter(video__id=video_id)
        return VideoIndex.objects.all()


from .models import Image
import cv2

import cv2
import numpy as np
from django.http import HttpResponse
import logging

logger = logging.getLogger(__name__)
@api_view(['GET']) 
def RenderView(request):
    logger.info("Received request")
    
    if request.method == "GET":
        parameter = request.GET.get('request')  # Fetch the 'parameter' from query string
        
        if parameter == "POLYGON":
            try:
                # Extract 'image_id' and 'element' from the request
                image_id = int(request.GET.get('image_id'))
                element = request.GET.get('element') 
                logger.info(f"Parameter: {parameter}, Image ID: {image_id}, Element: {element}")
                
                # Fetch the image and polygon data
                image_obj = Image.objects.filter(id=image_id).first()
                element_obj = Element.objects.filter(name=element).first()
                
                if not image_obj or not element_obj:
                    logger.error("Image or Annotation not found")
                    return HttpResponse("Image or Annotation not found", status=404)

                # Get all annotations for the specified image and element
                polygon_shapes = [
                    json.loads(obj.region_coord) for obj in Annotation.objects.filter(image=image_obj, element=element_obj)
                ] 

                # Load the image
                image_file = image_obj.file.path  # Assuming this is a path to the file
                logger.info(f"Loading image from: {image_file}")
                image = cv2.imread(image_file)
                
                if image is None:
                    logger.error("Failed to load image")
                    return HttpResponse("Failed to load image", status=500)

                # Draw each polygon on the image
                color = (0, 0, 255)  # Red color in BGR
                thickness = 10  # Thickness of the polygon border

                for polygon_shape in polygon_shapes:
                    try:
                        polygon = np.array(polygon_shape, np.int32)  # Convert polygon points to numpy array
                        polygon = polygon.reshape((-1, 1, 2))  # Reshape to the format OpenCV expects
                        cv2.polylines(image, [polygon], isClosed=True, color=color, thickness=thickness)
                    except Exception as e:
                        logger.error(f"Error drawing polygon: {str(e)}")

                # Encode image as PNG and send as HTTP response
                success, buffer = cv2.imencode('.png', image)
                if not success:
                    logger.error("Failed to encode image")
                    return HttpResponse("Failed to process image", status=500)
                
                return HttpResponse(buffer.tobytes(), content_type="image/png")
            
            except (TypeError, ValueError, json.JSONDecodeError) as e:
                logger.error(f"Error parsing request data: {str(e)}")
                return HttpResponse("Invalid request data", status=400)
        
        else:
            logger.warning(f"Invalid parameter: {parameter}")
            return HttpResponse("Unsupported parameter value", status=400)
    
    logger.error(f"Unsupported HTTP method: {request.method}")
    return HttpResponse("Unsupported HTTP method", status=405)


from django.http import StreamingHttpResponse, Http404
import os
@api_view(["GET", "HEAD"])
def stream_video(request, bridge, video):
    logger.info(f"Streaming video - Bridge: {bridge}, Video: {video}")
    
    # Construct the file path
    file_path = f"/app/bridgenet/media/videos/{bridge}/{video}"  # Update with your actual base path

    if not os.path.exists(file_path):
        raise Http404("Video file not found.")

    # Get file size
    file_size = os.path.getsize(file_path)
    
    # Handle the 'Range' header if present
    range_header = request.headers.get('Range', None)
    if range_header:
        logger.info(f"Range header: {range_header}")
        
        # Parse the range header
        start, end = range_header.replace('bytes=', '').split('-')
        start = int(start)
        end = int(end) if end else file_size - 1
        length = end - start + 1

        # Create a file iterator for the requested range
        def file_iterator(file_path, start, end, chunk_size=8192):
            with open(file_path, 'rb') as f:
                f.seek(start)
                bytes_to_read = end - start + 1
                while bytes_to_read > 0:
                    chunk = f.read(min(chunk_size, bytes_to_read))
                    if not chunk:
                        break
                    yield chunk
                    bytes_to_read -= len(chunk)

        response = StreamingHttpResponse(
            file_iterator(file_path, start, end),
            status=206,  # Partial Content
            content_type="video/mp4"
        )
        response['Content-Range'] = f'bytes {start}-{end}/{file_size}'
        response['Content-Length'] = str(length)
    else:
        # Serve the entire file if no range is specified
        def file_iterator(file_path, chunk_size=8192):
            with open(file_path, 'rb') as f:
                while chunk := f.read(chunk_size):
                    yield chunk

        response = StreamingHttpResponse(
            file_iterator(file_path),
            content_type="video/mp4"
        )
        response['Content-Length'] = str(file_size)

    # Add common headers
    response['Accept-Ranges'] = 'bytes'
    response['Content-Disposition'] = f'inline; filename="{video}"'
    
    return response

from api.serializers import FlatVideoIndexSerialzer
from api.models import FlatVideoIndex

class FlatVideoIndexListCreate(viewsets.ModelViewSet):
    serializer_class = FlatVideoIndexSerialzer

    def get_queryset(self):
        bridge_id = self.request.query_params.get('bridge')
        element_id = self.request.query_params.get('element')

        params = {}
        if bridge_id:
            params["bridge"] = bridge_id
        if element_id:
            params["element"] = element_id

        return FlatVideoIndex.objects.filter(**params)
