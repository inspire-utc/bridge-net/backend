"""
This worker does nothing. It is used for testing purposes. It should also be used
as a guide for creating new workers
"""

from celery import Celery
import time
import os

app = Celery(__name__,  broker='redis://redis:6379/0', backend="redis://redis:6379/1")


@app.task(
        name="mock", 
        queue="mock_queue", # Each worker gets its own queue in the redis database
) 
def mock(input_data, workspace_path):
    """
    A worker works in its own docker container. It has no access to Django models
    
    :param input_data: The input data for this worker. It is a dictionary
    :param workspace_path: The path to the workspace directory. Use this directory 
        to store computational results. It is managed by the django app
    :return: A dictionary containing the result of the worker
    """
    assert os.path.exists(workspace_path)


    print("Mock worker started")
    time.sleep(5) # Pretend to do some computation

    # Write out the result to a file
    filename = "mock_result.txt"
    with open(workspace_path + "/" + filename, "w") as f:
        f.write("This is a mock result")
    result = os.path.join(workspace_path, filename)
    
    print("Mock worker finished")
    
    return {
        "result": result,
        "url": "http://localhost:8000" + result
    }
