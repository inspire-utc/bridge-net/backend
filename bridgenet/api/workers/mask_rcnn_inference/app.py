import os
import sys


def mask_rcnn(input_data, workspace=None):
    # add mask-rcnn submodule to python path so we can import it
    sys.path.append(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), "pytorch-mask-rcnn")
    )

    import pytorch_mask_rcnn as pmr
    import torch
    import json
    import numpy as np
    import os
    import json
    import cv2

    USE_CUDA = True
    DATASET = "coco"
    CKPT_PATH = "weights/weights.pth"
    DATA_DIR = "bridge-data"

    # Set the device to CUDA if gpu is available
    device = torch.device("cuda" if torch.cuda.is_available() and USE_CUDA else "cpu")
    if device.type == "cuda":
        pmr.get_gpu_prop(show=True)
    print("\ndevice: {}".format(device))

    # Load the dataset that the model was trained with. We need to make sure that
    # the number of classes match the one with weights

    ds = pmr.datasets(DATASET, DATA_DIR, "test", train=False)
    d = torch.utils.data.DataLoader(ds, shuffle=False)

    model = pmr.maskrcnn_resnet50(True, max(ds.classes) + 1).to(device)
    model.eval()
    model.head.score_thresh = 0.7

    if CKPT_PATH:
        checkpoint = torch.load(CKPT_PATH, map_location=device)
        model.load_state_dict(checkpoint["model"])
        print(checkpoint["eval_info"])
        del checkpoint


    # We need the annotation data to do lookups
    with open(os.path.join(DATA_DIR, "train", "_annotations.coco.json")) as fp:
        ANNOTATION_TABLE = json.load(fp)


    def get_label_from_id(label_id):
        return ANNOTATION_TABLE["categories"][label_id].copy()

        
    image_folder = input_data
    with open("bridge-data/train/_annotations.coco.json") as fp:
        annotation = json.load(fp)

    # Don't need the annotations that goes with the specific images
    del annotation["annotations"]
    
    images = annotation["images"] = []
    job_status = {}
    for img_path in os.listdir(image_folder):
        if img_path.endswith(".json"):
            continue
        image = cv2.imread(os.path.join(image_folder, img_path))
        height, width, _ = image.shape
        images.append(
            {
                "id": len(images),
                "file_name": img_path,
                "width": width,
                "height": height,
            }
        )

    # Create '_annotations.coco.json' file
    with open(os.path.join(image_folder, "_annotations.coco.json"), "w") as f:
        json.dump(annotation, f, indent=2)

    dataset = pmr.datasets(DATASET, image_folder, ".", train=False)
    d = torch.utils.data.DataLoader(dataset, shuffle=False)
    inference_dir = workspace

    image_paths = [
        img
        for img in os.listdir(image_folder)
        if not img.endswith(".json")
    ]

    with torch.no_grad():
        for (image, target), name in zip(d, image_paths):
            image = image.to(device)[0]
            result = model(image)
            
            # We found something interesting...
            if len(result["labels"]):
                # TODO add a link to inference result
                # TODO save the model result with mask and label information
                
                if inference_dir:
                    pmr.show(
                        image,
                        result,
                        ds.classes,
                        save_path=os.path.join(inference_dir, name),
                    )
                    
                # result["labels"] returns a list of primary keys that refers
                # to the segmentation class. So we need to do a lookup                
                status = job_status[name] = {}
                status["status"] = "Done"
                labels = status["labels"] = []

                # Iterate over each semantic instance found in this image
                for box, mask, id in zip(result["boxes"], result["masks"], result["labels"]):
                    label = get_label_from_id(id)
                    label["box"] = box.tolist()

                    # Extract polygon from mask data
                    if mask.is_floating_point():
                        mask = mask > 0.5
                    m = np.asarray(mask.cpu())
                    polygon = pmr.GenericMask(m, image.shape[1], image.shape[0]).polygons
                    label["polygons"] = [seg.reshape(-1, 2).tolist() for seg in polygon]
                    labels.append(label)

    return job_status


if __name__ == "__main__":
    import sys
    print(mask_rcnn(sys.argv[1]))