import json
import sys

def map_category_ids_to_names():
    # Load the COCO file
    
    coco_data = json.load(sys.stdin)

    # Create a mapping of category_id to name
    category_map = {category['id']: category['name'] for category in coco_data['categories']}
    
    # Iterate through annotations and map category_ids to names
    mapped_data = set()
    for annotation in coco_data['annotations']:
        # Check if the segmentation is a polygon (a list of arrays)
        if isinstance(annotation.get('segmentation'), list):
            category_id = annotation['category_id']
            category_name = category_map.get(category_id, "Unknown")
            mapped_data.add(category_name)

    return mapped_data

# Example usage
mapped_categories = map_category_ids_to_names()
print(mapped_categories)
