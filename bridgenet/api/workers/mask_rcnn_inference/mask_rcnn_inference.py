"""
This worker does nothing. It is used for testing purposes. It should also be used
as a guide for creating new workers
"""

from celery import Celery
import json


app = Celery(__name__,  broker='redis://redis:6379/0', backend="redis://redis:6379/1")


@app.task(
        name="mask_rcnn_inference", 
        queue="mask_rcnn_inference_queue", # Each worker gets its own queue in the redis database
) 
def mask_rcnn_inference(input_data, workspace):
    """
    A worker works in its own docker container. It has no access to Django models
    
    :param input_data: The input data for this worker. It is a dictionary
    :param workspace_path: The path to the workspace directory. Use this directory 
        to store computational results. It is managed by the django app
    :return: A dictionary containing the result of the worker
    """
    import time
    import os
    import sys
    import json

    assert os.path.exists(workspace)

    print(f"Start mask_rcnn_inference with input: {json.dumps(input_data, indent=4)}")
    
    if "frame_data" in input_data: # Received from split frame
        image_folder = input_data["frame_path"]
        input_data["images"] = [
            { 
                "id": id, 
                "file": frame["frame_path"], 
                "url": frame["frame_url"], 
                "frame_no": frame["frame_no"], 
                "frame_sec": frame["frame_sec"] 
            } for (id, frame) in enumerate(input_data["frame_data"])
        ]

    elif "bridge" in input_data: # User wants to do inference on all images of a bridge
        image_folder = os.path.join("/", "media", "images", str(input_data["bridge"]))
        input_data["images"] = [
            { "id": id, "file": os.path.join(image_folder, file), "url": os.path.join(image_folder, file) } for (id, file) in enumerate(os.listdir(image_folder))
        ]
        print("Performing inference on Bridge Images")
        print(json.dumps(input_data, indent=2))
    elif "images" in input_data:
        image_folder = get_base_directory(input_data)
    else:
        print(json.dumps(input_data,indent=4))
        raise ValueError("Input data must contain 'images' key")
    
    lookup_table = create_lookup_table(input_data)

    assert lookup_table, f"{lookup_table}"
    
    sys.path.append(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), "pytorch-mask-rcnn")
    )
    # -- init pytorch model -- #

    import pytorch_mask_rcnn as pmr
    import torch
    import numpy as np
    import os
    import cv2

    USE_CUDA = True
    DATASET = "coco"
    CKPT_PATH = "weights/weights.pth"
    DATA_DIR = "bridge-data"

    # Set the device to CUDA if gpu is available
    device = torch.device("cuda" if torch.cuda.is_available() and USE_CUDA else "cpu")
    if device.type == "cuda":
        pmr.get_gpu_prop(show=True)
    print("\ndevice: {}".format(device))

    # Load the dataset that the model was trained with. We need to make sure that
    # the number of classes match the one with weights

    ds = pmr.datasets(DATASET, DATA_DIR, "test", train=False)
    d = torch.utils.data.DataLoader(ds, shuffle=False)

    model = pmr.maskrcnn_resnet50(True, max(ds.classes) + 1).to(device)
    model.eval()
    model.head.score_thresh = 0.5

    if CKPT_PATH:
        checkpoint = torch.load(CKPT_PATH, map_location=device)
        model.load_state_dict(checkpoint["model"])
        print(checkpoint["eval_info"])
        del checkpoint

    # -- Load Annotation Table -- #

    # We need the annotation data to do lookups
    with open(os.path.join(DATA_DIR, "train", "_annotations.coco.json")) as fp:
        ANNOTATION_TABLE = json.load(fp)

    def get_label_from_id(label_id):
        return ANNOTATION_TABLE["categories"][label_id].copy()

    with open("bridge-data/train/_annotations.coco.json") as fp:
        annotation = json.load(fp)

    # Don't need the annotations that goes with the specific images
    del annotation["annotations"]
    
    images = annotation["images"] = []
    job_status = {}
    for img_path in os.listdir(image_folder):
        if img_path.endswith(".json"):
            continue
        image = cv2.imread(os.path.join(image_folder, img_path))
        height, width, _ = image.shape
        images.append(
            {
                "id": len(images),
                "file_name": img_path,
                "width": width,
                "height": height,
            }
        )

    # Create '_annotations.coco.json' file
    with open(os.path.join(image_folder, "_annotations.coco.json"), "w") as f:
        json.dump(annotation, f, indent=2)

    # -- Load Data set -- #
    dataset = pmr.datasets(DATASET, image_folder, ".", train=False)
    d = torch.utils.data.DataLoader(dataset, shuffle=False)
    inference_dir = workspace

    image_paths = [
        img
        for img in os.listdir(image_folder)
        if not img.endswith(".json")
    ]

    # -- Run model -- #

    with torch.no_grad():
        for (image, target), name in zip(d, image_paths):
            image = image.to(device)[0]
            result = model(image)
            
            # We found something interesting...
            if len(result["labels"]):
                # TODO add a link to inference result
                # TODO save the model result with mask and label information
                
                if inference_dir:
                    pmr.show(
                        image,
                        result,
                        ds.classes,
                        save_path=os.path.join(inference_dir, name),
                    )
                    
                # result["labels"] returns a list of primary keys that refers
                # to the segmentation class. So we need to do a lookup                
                status = job_status[name] = { 
                    "image_id": lookup_table[name]["id"],
                    "image_url": lookup_table[name]["url"],
                    "frame_no": lookup_table[name].get("frame_no"),
                    "frame_sec": lookup_table[name].get("frame_sec")
                }
                status["status"] = "Done"
                labels = status["labels"] = []

                # Iterate over each semantic instance found in this image
                for box, mask, id in zip(result["boxes"], result["masks"], result["labels"]):
                    label = get_label_from_id(id)
                    label["box"] = box.tolist()

                    # Extract polygon from mask data
                    if mask.is_floating_point():
                        mask = mask > 0.5
                    m = np.asarray(mask.cpu())
                    polygon = pmr.GenericMask(m, image.shape[1], image.shape[0]).polygons
                    label["polygons"] = [seg.reshape(-1, 2).tolist() for seg in polygon]
                    labels.append(label)
            else:
                print(f"Nothing found for {name}")
    
    result = {
        "result": job_status,
        "frame_data": input_data.get("frame_data"),
        "bridge": input_data.get("bridge"),
        "video": input_data.get("video"),
        
    }

    with open(os.path.join(workspace, "result.json"), "w") as fp:
        json.dump(result, fp, indent=2)
    
    return result

def get_base_directory(data):
    if 'images' not in data or not isinstance(data['images'], list) or len(data['images']) == 0:
        return None  # or raise an error

    # Assuming 'file' key exists and extracting the base directory from the first image
    base_directory = '/'.join(data['images'][0]['file'].split('/')[:-1])

    # Validate that all images have the same base directory
    for image in data['images']:
        if 'file' not in image:
            return None  # or raise an error
        current_directory = '/'.join(image['file'].split('/')[:-1])
        if current_directory != base_directory:
            return None  # or raise an error

    return base_directory

def create_lookup_table(data):
    if 'images' not in data or not isinstance(data['images'], list):
        return None  # or raise an error

    print(data["images"])

    lookup_table = {}
    for image in data['images']:
        if 'file' in image and 'id' in image:
            # Extract filename from the 'file' path
            filename = image['file'].split('/')[-1]
            lookup_table[filename] = image

    return lookup_table