from celery import Celery
import typing
from urllib.parse import urlparse
import os
import uuid

app = Celery(__name__,  broker='redis://redis:6379/0', backend="redis://redis:6379/1")

# We need to specify
# - name because we are in a docker container so we need to tell celery the exaxt name to use
# - queue because multiple docker containers


class InputData(typing.TypedDict):
    bridge: str
    video: str
    nframes: int

@app.task(name="split_video_task", queue="split_video_queue")
def split_video(input_data: InputData, workspace: str):

    import cv2
    import json
    import os
    from urllib.parse import urlparse

    print("split_video received")
    print(json.dumps(input_data, indent=4))

    bridge = input_data["bridge"]
    url = urlparse(input_data["video"])
    n_seconds = input_data["nth_second"]
    path = url.path
    output_folder = workspace

    extracted_url = f"{url.scheme}://{url.netloc}"

    # Assert that the video file exists
    assert os.path.exists(path), f"The video file at path {path} does not exist."

    # Capture the video from the file
    cap = cv2.VideoCapture(path)

    # Get the frame rate of the video
    fps = cap.get(cv2.CAP_PROP_FPS)
    frames_to_skip = int(fps * n_seconds)

    frame_list = []
    frame_count = 0
    saved_frame_count = 0

    while cap.isOpened():
        ret, frame = cap.read()
        # If the frame was read correctly ret is True
        if not ret:
            break
        
        if frame_count % frames_to_skip == 0:
            # Save each nth second frame as an image file
            frame_filename = os.path.join(output_folder, f"frame_{frame_count:04d}.jpg")
            cv2.imwrite(frame_filename, frame)

            frame_info = {
                "frame_no": frame_count,
                "frame_path": frame_filename,
                "frame_url": extracted_url + frame_filename,
                "frame_sec": frame_count / fps
            }

            frame_list.append(frame_info)
            saved_frame_count += 1

        frame_count += 1

    # Release the capture after everything is done
    cap.release()

    result = {
        "bridge": bridge,
        "video": input_data["video"],
        "frame_url": extracted_url + output_folder,
        "frame_path": output_folder,
        "frame_data": frame_list,
    }

    print("split_video done")
    print(json.dumps(result, indent=2))
    return result
