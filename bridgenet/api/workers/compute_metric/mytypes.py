import typing

class Annotation(typing.TypedDict):
    id: int
    region_name: str
    region_coord: list[tuple[int, int]]
    region_share: str

class GroundTruthElement(typing.TypedDict):
    image_id: int
    image_file: str
    labels: list[Annotation]
