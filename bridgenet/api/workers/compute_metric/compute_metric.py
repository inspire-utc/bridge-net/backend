"""
This worker does nothing. It is used for testing purposes. It should also be used
as a guide for creating new workers
"""

from celery import Celery
import time
import os
import json
import typing

if typing.TYPE_CHECKING:
    from mytypes import *

app = Celery(__name__,  broker='redis://redis:6379/0', backend="redis://redis:6379/1")




@app.task(
        name="compute_metric", 
        queue="compute_metric_queue", # Each worker gets its own queue in the redis database
) 
def compute_metric(input_data, workspace_path):
    """
    A worker works in its own docker container. It has no access to Django models
    
    :param input_data: The input data for this worker. It is a dictionary
    :param workspace_path: The path to the workspace directory. Use this directory 
        to store computational results. It is managed by the django app
    :return: A dictionary containing the result of the worker
    """
    import shapely    
    assert os.path.exists(workspace_path)
    from shapely.ops import unary_union
    from shapely import Polygon


    def get_image_by_id(result: dict, image_id: int):
        for k, v in result.items():
            if v["image_id"] == image_id:
                return v
        return None

    def get_polygon_by_name(image_result: dict, name: str):
        labels = image_result["labels"]
        grouped_labels = [
            label for label in labels if label["name"] == name
        ]
        polygons = []
        for label in grouped_labels:
            polygons.append([ shapely.Polygon(p) for p in label["polygons"]])
        return polygons

    def compute_metric_for_multisegment_label(polygons: list[Polygon], ground_truth_polygon: Polygon):
        if not polygons:
            return {
                "iou": 0,
                "f1": 0,
                "prec": 0,
                "recall": 0
            }
        
        # Compute the union of all prediction polygons
        predicted_union = unary_union(polygons)
        
        # Intersection of the union of predictions with the ground truth
        intersection = predicted_union.intersection(ground_truth_polygon).area
        
        # Union of the union of predictions with the ground truth
        union = predicted_union.union(ground_truth_polygon).area
        
        # Total area of the predictions and ground truth
        total_prediction_area = predicted_union.area
        ground_truth_area = ground_truth_polygon.area
        
        # Calculate metrics
        iou = intersection / union if union != 0 else 0
        precision = intersection / total_prediction_area if total_prediction_area != 0 else 0
        recall = intersection / ground_truth_area if ground_truth_area != 0 else 0
        f1 = 2 * (precision * recall) / (precision + recall) if (precision + recall) != 0 else 0
        
        return {
            "iou": iou,
            "f1": f1,
            "prec": precision,
            "recall": recall
        }


    def create_metrics_dict():
        return {
            "iou": [],
            "f1": [],
            "prec": [],
            "recall": []
        }

    def create_mean_metrics():
        return {
            "iou": 0,
            "f1": 0,
            "prec": 0,
            "recall": 0,
        }

    result = []

    model_results = input_data["result"]

    print([ model["image_id"] for model in model_results.values() ])

    ground_truth: list['GroundTruthElement'] = input_data["Ground Truth"]
    for data in ground_truth:
        image_id = data['image_id']
        image_file = data["image_file"]
        image_labels = data["labels"]



        image_result = get_image_by_id(model_results, image_id)
        if not image_result and not image_labels: # no nnotation:
            print(f"No images for {image_id}:{image_file}")
            continue
            
        
        metrics = {
            "image_id": image_id,
            "label_metrics": {},
            "mean_label_metrics": {}
        }

        label_metrics = metrics["label_metrics"]
        mean_label_metrics = metrics["mean_label_metrics"]


        if not image_result:
            # Handle zero prediction
            continue
            
        for label in image_labels:
            region_name = label["region_name"]
            region_coord = label["region_coord"]
            region_share = label["region_share"]
            assert region_share == "polygon" or region_share == "polyline"

            if region_name not in label_metrics:
                label_metrics[region_name] = create_metrics_dict()
                mean_label_metrics[region_name] = create_mean_metrics()
                
            
            region_metric = label_metrics[region_name]


            ground_truth_polygon = shapely.Polygon(region_coord)
            inference_polygons: list[list[shapely.Polygon]] = get_polygon_by_name(image_result, region_name)
            metrics_for_this_gt = [
                compute_metric_for_multisegment_label(polygon, ground_truth_polygon) for polygon in inference_polygons
            ]

            iou_max = max(metric["iou"] for metric in metrics_for_this_gt) if metrics_for_this_gt else 0
            f1_max = max(metric["f1"] for metric in metrics_for_this_gt) if metrics_for_this_gt else 0
            recall = max(metric["recall"] for metric in metrics_for_this_gt) if metrics_for_this_gt else 0
            prec = max(metric["prec"] for metric in metrics_for_this_gt) if metrics_for_this_gt else 0

            region_metric["iou"].append(iou_max)
            region_metric["f1"].append(f1_max)
            region_metric["recall"].append(recall)
            region_metric["prec"].append(prec)



        for segment in label_metrics:
            for key in ["iou", "f1", "prec", "recall"]:
                mean_label_metrics[segment][key] = sum(label_metrics[segment][key]) / len(label_metrics[segment][key]) if label_metrics[segment][key] else None
    
        result.append(metrics)

    print(result)
    return {
        "Compute Metric": {
            "Model": None,
            "Metrics": result,
        },
    }
