"""
This worker does nothing. It is used for testing purposes. It should also be used
as a guide for creating new workers
"""

from celery import Celery
import time
import os
from celery.utils.log import get_logger
import sys
import json
from typing import *

app = Celery(__name__,  broker='redis://redis:6379/0', backend="redis://redis:6379/1")
logger = get_logger(__name__)

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__))))

@app.task(
        name="multitask", 
        queue="multitask_queue", # Each worker gets its own queue in the redis database
) 
def multitask(input_data, workspace_path):
    """
    A worker works in its own docker container. It has no access to Django models
    
    :param input_data: The input data for this worker. It is a dictionary
    :param workspace_path: The path to the workspace directory. Use this directory 
        to store computational results. It is managed by the django app
    :return: A dictionary containing the result of the worker
    """
    assert os.path.exists(workspace_path)    
    from predict_MTL_D import main
    import numpy as np

    option = input_data.get("option")
    bridge = input_data.get("bridge")

    element_segmentation_path = os.path.join(workspace_path, "element_segmentation")
    damage_segmentation_path = os.path.join(workspace_path, "damage_segmentation")

    if bridge:
        image_path = f"/media/images/{bridge}"
        image_polygons = main(image_path, element_segmentation_path, damage_segmentation_path)
        element_as_vgg = {}
        damage_as_vgg = {}

        for image_name, (element_polygon, damage_polygon) in image_polygons.items():
            regions_e = []
            # Process each label for element polygons.
            for label, polygons in element_polygon.items():
                for poly in polygons:
                    # Remove any singleton dimensions (cv2 contours are often shape (N,1,2)).
                    poly = np.squeeze(poly)
                    # Ensure that poly is 2D (in case there is only one point, expand dims).
                    if poly.ndim == 1:
                        poly = np.expand_dims(poly, axis=0)
                    all_points_x = poly[:, 0].tolist()
                    all_points_y = poly[:, 1].tolist()
                    region = {
                        "shape_attributes": {
                            "name": "polygon",
                            "all_points_x": all_points_x,
                            "all_points_y": all_points_y
                        },
                        "region_attributes": {
                            "label": label
                        }
                    }
                    regions_e.append(region)
            
            regions_d = []
            # Process each label for damage polygons.
            for label, polygons in damage_polygon.items():
                for poly in polygons:
                    poly = np.squeeze(poly)
                    if poly.ndim == 1:
                        poly = np.expand_dims(poly, axis=0)
                    all_points_x = poly[:, 0].tolist()
                    all_points_y = poly[:, 1].tolist()
                    region = {
                        "shape_attributes": {
                            "name": "polygon",
                            "all_points_x": all_points_x,
                            "all_points_y": all_points_y
                        },
                        "region_attributes": {
                            "label": label
                        }
                    }
                    regions_d.append(region)
            
            element_image_path_for_vgg = os.path.join(element_segmentation_path, image_name)
            damage_image_path_for_vgg = os.path.join(damage_segmentation_path, image_name)
            # Create a VGG-style annotation entry for elements.
            element_as_vgg[element_image_path_for_vgg] = {
                "filename": element_image_path_for_vgg,
                "size": 0,  # Update this if you have the actual file size.
                "regions": regions_e,
                "file_attributes": {}
            }
            # Create a VGG-style annotation entry for damages.
            damage_as_vgg[damage_image_path_for_vgg] = {
                "filename": damage_image_path_for_vgg,
                "size": 0,  # Update with actual file size if available.
                "regions": regions_d,
                "file_attributes": {}
            }

            # write to {element_segmentation_path}/annotation.json
            with open(os.path.join(element_segmentation_path, "annotation.json"), "w") as f:
                json.dump(element_as_vgg, f)
            # write to {damage_segmentation_path}/annotation.json
            with open(os.path.join(damage_segmentation_path, "annotation.json"), "w") as f:
                json.dump(damage_as_vgg, f)
            


    else: 
        raise ValueError("Invalid option")

    return {
        "element_segmentation": [ os.path.join(element_segmentation_path, image) for image in os.listdir(element_segmentation_path) if not image.endswith("json") ],
        "damage_segmentation":  [ os.path.join(damage_segmentation_path, image) for image in os.listdir(damage_segmentation_path) if not image.endswith("json") ],
        "element_vgg_file": os.path.join(element_segmentation_path, "annotation.json"),
        "damage_vgg_file": os.path.join(damage_segmentation_path, "annotation.json"),
        "element_vgg": element_as_vgg,
        "damage_vgg": damage_as_vgg
    }
