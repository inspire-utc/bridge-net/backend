import time
import os
import torch
import cv2
import numpy as np
from PIL import Image

from hrnet_multi_MTL_D import HRnet_Segmentation

def main(dir_origin_path, dir_save_path_e, dir_save_path_d):
    hrnet = HRnet_Segmentation()
    mode            = "dir_predict"
    count           = False
    name_classes    = [["_background_","None","Barrier","Bearing","Deck","Diaphragm", "Girder", "Pier+Abutment(Substructure)"], ["_background_","Corrosion","Crack"]]  # I changed
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    cross_e = torch.load('logs/cross_e-best_epoch_weights.pth', map_location=device)
    cross_d = torch.load('logs/cross_d-best_epoch_weights.pth', map_location=device)
    video_path      = 0
    video_save_path = ""
    video_fps       = 25.0
    test_interval = 100
    fps_image_path  = ""
    simplify        = True
    onnx_save_path  = "./model_data/models.onnx"

    if mode == "predict":
        while True:
            img = input('Input image filename:')
            try:
                image = Image.open(img)
            except:
                print('Open Error! Try again!')
                continue
            else:
                r_image = hrnet.detect_image(image, count=count, name_classes=name_classes)
                r_image.show()

    elif mode == "video":
        capture=cv2.VideoCapture(video_path)
        if video_save_path!="":
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            size = (int(capture.get(cv2.CAP_PROP_FRAME_WIDTH)), int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
            out = cv2.VideoWriter(video_save_path, fourcc, video_fps, size)

        ref, frame = capture.read()
        if not ref:
            raise ValueError("Failed")

        fps = 0.0
        while(True):
            t1 = time.time()
            ref, frame = capture.read()
            if not ref:
                break
            frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
            frame = Image.fromarray(np.uint8(frame))
            frame = np.array(hrnet.detect_image(frame))
            frame = cv2.cvtColor(frame,cv2.COLOR_RGB2BGR)
            
            fps  = ( fps + (1./(time.time()-t1)) ) / 2
            print("fps= %.2f"%(fps))
            frame = cv2.putText(frame, "fps= %.2f"%(fps), (0, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            
            cv2.imshow("video",frame)
            c= cv2.waitKey(1) & 0xff 
            if video_save_path!="":
                out.write(frame)

            if c==27:
                capture.release()
                break
        print("Video Detection Done!")
        capture.release()
        if video_save_path!="":
            print("Save processed video to the path :" + video_save_path)
            out.release()
        cv2.destroyAllWindows()

    elif mode == "fps":
        img = Image.open(fps_image_path)
        tact_time = hrnet.get_FPS(img, test_interval)
        print(str(tact_time) + ' seconds, ' + str(1/tact_time) + 'FPS, @batch_size 1')
        
    elif mode == "dir_predict":
        import os
        from tqdm import tqdm

        img_names = os.listdir(dir_origin_path)
        image_polygons = {}
        for i, img_name in enumerate(img_names):
            if img_name.lower().endswith(('.bmp', '.dib', '.png', '.jpg', '.jpeg', '.pbm', '.pgm', '.ppm', '.tif', '.tiff', '.JPEG', '.JPG')):
                image_path  = os.path.join(dir_origin_path, img_name)
                image       = Image.open(image_path)
                element_img, element, damage_img, damage = hrnet.detect_image(image, cross_e, cross_d, count=True, name_classes=name_classes)
                if not os.path.exists(dir_save_path_e):
                    os.makedirs(dir_save_path_e)
                if not os.path.exists(dir_save_path_d):
                    os.makedirs(dir_save_path_d)
                element_img.save(os.path.join(dir_save_path_e, img_name))
                damage_img.save(os.path.join(dir_save_path_d, img_name))
                print(f"{i+1} / {len(img_names)} {img_name}:")
                image_polygons[img_name] = [ element, damage ]
            
        return image_polygons
    elif mode == "export_onnx":
        hrnet.convert_to_onnx(simplify, onnx_save_path)
        
    else:
        raise AssertionError("Please specify the correct mode: 'predict', 'video', 'fps' or 'dir_predict'.")

if __name__ == "__main__":
    main()
