FROM python:3.7-slim

WORKDIR /

RUN apt-get update
RUN apt-get install -y libglib2.0
RUN apt-get install -y libsm6 libxext6 libxrender-dev libx264-dev libjpeg-dev

RUN pip3 install --upgrade torch torchvision

COPY requirements.txt /requirements.txt
RUN pip3 install -r requirements.txt