# This file only exists to override the default rest_framework TokenAuthentication
# with our own to allow multiple tokens per user

from rest_framework.authentication import TokenAuthentication

from api.models import MultiToken


class MultiTokenAuthentication(TokenAuthentication):
    model = MultiToken
