"""
Module defines Data Transfer Objects
"""
from rest_framework import serializers

class WorkItemRequestDTO(serializers.Serializer):
    """
    User request a specific workitem to be done
    """
    # if job_id, this workitem is an independent work item
    job_id = serializers.UUIDField(required=False)
    type = serializers.CharField() # Type of worker
    input_data = serializers.JSONField(required=False) # Input Data