"""bridgenet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from api import views


router = routers.DefaultRouter()
router.register(r"users", views.UserView, "user")
router.register(r"bridges", views.BridgeView, "bridge")
router.register(r"elements", views.ElementView, "element")
router.register(r"conditions", views.ConditionView, "condition")
router.register(r"images", views.ImageUploadView, "image")
router.register(r"videos", views.VideoUploadView, "video")
router.register(r"annotations", views.AnnotationView, "annotation")
router.register(r"workflows", views.WorkflowView, "workflow")
router.register(r"workitems", views.WorkItemView, "workitem")
router.register(r"workflowjobs", views.WorkflowJobViewSet, "workflowjobs")
router.register(r"image-element", views.ImageElementView, "image-element")
router.register(r"video-index", views.VideoIndexListCreate, "video-index")
router.register(r"flat-index", views.FlatVideoIndexListCreate, "flat-video-index")

urlpatterns = [
    path("admin/", admin.site.urls),
    path("annotator/", views.Annotator),
    path("annotator/<int:bridge>", views.Annotator),
    path("api/", include(router.urls)),
    path("auth/", views.LoginApi.as_view()),
    path("annotated-image/<int:image>", views.AnnotatedImage),
    path("workflows/<str:guid>", views.WorkflowView.as_view({"get": ["get_workflow_by_id"]})),
    path("jobs/", views.JobView),
    path('jobs/create_workitem', views.create_workitem, name='create_workitem'),
    path('render/workitems/<str:workitem_id>', views.workitem_render, name='workitem_render'),
    path('render/image', views.RenderView, name="image_render"),
    path('update-elements/<str:annotation_id>/', views.update_elements, name='update_elements'),
    path('media/videos/<int:bridge>/<str:video>', views.stream_video, name="stream_video"),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)