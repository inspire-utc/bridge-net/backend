
# Create a post request to create a job
# To get the Auth token, login to bridgenet and copy this into the console
# sessionStorage.getItem("user-info")

TOKEN="e2b707190f5270b47db3d91237e4a7ebcb2682d2"

curl -X POST http://localhost:8000/jobs/create_workitem \
-H "Content-Type: application/json" \
-H "Authorization: Token $TOKEN" \
-d '{ "type": "framediff", "input_data": {"key": "value"} }'
