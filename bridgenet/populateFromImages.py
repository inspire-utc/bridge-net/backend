from api.models import Image, Bridge
import shutil
import os as OS

states = {
    "AK": 1,
    "AL": 2,
    "AR": 5,
    "AZ": 4,
    "CA": 6,
    "CO": 8,
    "CT": 9,
    "DC": 11,
    "DE": 10,
    "FL": 12,
    "GA": 13,
    "HI": 15,
    "IA": 19,
    "ID": 16,
    "IL": 17,
    "IN": 18,
    "KS": 20,
    "KY": 21,
    "LA": 22,
    "MA": 25,
    "MD": 24,
    "ME": 23,
    "MI": 26,
    "MN": 27,
    "MO": 29,
    "MS": 28,
    "MT": 30,
    "NC": 37,
    "ND": 38,
    "NE": 31,
    "NH": 33,
    "NJ": 34,
    "NM": 35,
    "NV": 32,
    "NY": 36,
    "OH": 39,
    "OK": 40,
    "OR": 41,
    "PA": 42,
    "RI": 44,
    "SC": 45,
    "SD": 46,
    "TN": 47,
    "TX": 48,
    "UT": 49,
    "VA": 51,
    "VT": 50,
    "WA": 53,
    "WI": 55,
    "WV": 54,
    "WY": 56,
}

# assumed directory can be changed with args
IMAGE_DIRECTORY_PATH = "../database/BridgeNet"


# print the metadata of the given directory for testing
def printAllMetadata(directory):
    path = OS.walk(directory)
    for root, subdirs, files in path:
        for file in files:
            if root.find("MO2646") != -1:
                shutil.copy(
                    src=root + "/" + file, dst="./bridgenet/media/images/2646/" + file
                )
                image = Image(
                    file="images/2646/" + file,
                    bridge=Bridge.objects.get(structure_number=2646),
                )
                image.save()
            if root.find("MO_A4351") != -1:
                shutil.copy(
                    src=root + "/" + file, dst="./bridgenet/media/images/4351/" + file
                )
                image = Image(
                    file="images/4351/" + file,
                    bridge=Bridge.objects.get(structure_number=4351),
                )
                image.save()
        for subdir in subdirs:
            print(
                "\033[1m-------------"
                + subdir
                + "----------------------------------------------\033[0m"
            )
            printAllMetadata(root + subdir)


# # insert all the metadata of images into the database. Created a new collection for each image subdirectory
# def insertAllMetadata(directory, db):
#     path = OS.walk(directory)
#     for root, subdirs, files in path:
#         for file in files:
#             col = db[
#                 root.split("/")[-1]
#             ]  # name the collection as the name of the folder containing the images
#             db[root.split("/")[-1]].insert_one(getMetaData(root + "/" + file))
#         for subdir in subdirs:
#             col = db[subdir]


printAllMetadata(IMAGE_DIRECTORY_PATH)
