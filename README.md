# Bridgnet Backend

This repository is the backend for the bridgenet website. It uses django as its framework and a specialized pipenv container for development.

## Quickstart

1. If you don't already have Singularity on your system see [installing singularity](https://docs.sylabs.io/guides/3.5/admin-guide/installation.html).

2. Clone the repository to a folder of your choice and cd into it.

3. Use `singularity pull <my_container_name>.sif docker://twh53r/bridgenet:backend-1.0`

4. Use `singularity exec <my_container_name>.sif pipenv install`.

5. You can quickly get a database running with docker `docker run --name bn-postgres -e POSTGRES_USER=django -e POSTGRES_PASSWORD=<django_secret_password> -e POSTGRES_DB=bridgenet -d -p 5432:5432 postgres`

6. You should be good to start developing! See the rest of this readme if you do not know how to use any of the tools for this project.

## Container Tips

<!--This project uses a specialized container with pipenv installed by default. This container is hosted on dockerhub at <https://hub.docker.com/repository/docker/twh53r/bridgenet>. You can pull it using this name "twh53r/bridgenet". Make sure you use whatever the latest tag is. At time of writing, the full name and tag is "twh53r/bridgenet:backend-1.0". This docker repository does not use the latest tag in case completely different builds are hosted here in the future.-->

Run the docker compose file to set up the backend. **Create a `.env` file so the docker compose file can properly access the postgres database or modify the docker compose file accordingly.** Ask one of the people on the project for the secret key.

### Using Singularity

Currently, development is done using Singularity, not Docker for runtime development with the container. If you have a proper install of singularity, getting a working local version of the container should be as easy as `singularity pull <my_container_name>.sif docker://twh53r/bridgenet:<tag>`. You should then be able to use `singularity shell <my_container_name>.sif` to open a shell and execute any pipenv or other development related commands within it.

## Django Tips

To learn more about Django see their documentation: <https://docs.djangoproject.com/en/4.0/>

Anytime you need to use `django-admin`, use `python -m django` instead. Of course, this needs to be run within the pipenv virtual environment using `pipenv run <command>` or by opening a pipenv shell. More on that in [Pipenv Tips](#pipenv-tips).

### Navigating the Bridgenet Directory

All src code is under `bridgnet`.

- `bridgenet/bridgenet` is the project directory. The project is the overall managing entity in django. It connects to any databases, server hosting software, manages urls, and any other settings.
- `bridgenet/api` is the api app directory. For modifying the backend, this is the most important directory. To keep it simple it is best to think a project might have multiple apps. As an example, the project Amazon may have apps for shopping, watching videos, and listening to music. For a more in depth explanation, see [here](https://docs.djangoproject.com/en/4.0/ref/applications/).
  - Define any urls you create in urls.py.
  - All urls should have a view associated with them.

### Getting Started With Postgresql

- It is recommended to install pgadmin4 to view the database in real time: <https://www.pgadmin.org/download/>.
- You can quickly get a database running with docker `docker run --name bn-postgres -e POSTGRES_USER=django -e POSTGRES_PASSWORD=<django_secret_password> -e POSTGRES_DB=bridgenet -d -p 5432:5432 postgres`
- You can then connect to this database in pgadmin4 by specifying localhost in the connect section of add server, and setting password to password.
- I apologize that the database setup uses Docker given that everything else uses Singularity. This just provides a quick and easy setup that will always be up to date with the latest postgres. Setting up a custom Singularity definition file would likely add uneeded complexity. You are free to do so, though! Then, update this readme to explain how to use your Singularity container!

### Connecting to Database

Set your connection settings for the database in settings.py under databases. For more information on connecting to databases, see [here](https://docs.djangoproject.com/en/4.0/ref/settings/#std-setting-DATABASES).

### Modifying Models

Modifying models is perfectly safe, as long as you do not complete remove a field. Django with safely add fields and edit the table without losing data.
After changing a model, run the following to perform the migration on the database:

1. `manage.py makemigrations api`
2. `manage.py sqlmigrate api <migration_num>`
   - _This is optional, it lets you view the sql Django will use to perform themigration_
3. `manage.py migrate`

If you want to know the steps involved to modify models so that they work with our implementation of REST, see this [document](https://docs.google.com/document/d/16CisbmjR_c8GB2HwAdtPWkqO3-NBwnpd6tOzt4fNRe8/edit?usp=sharing) (OUT OF DATE). It explains everything involved in editing or even adding a new model, so that it is both queryable and modifiable through the api.

## Pipenv Tips

Pipenv makes module and venv management easy as they are one in the same (as they should be). Pipenv is already installed within the container, so there is no need to worry about installation.

### Useful Commands

- `pipenv run <command>`

  - This runs any given command within the venv. You will need to use it for almost any command.

- `pipenv shell`

  - If you do not want to repeatedly use `pipenv run`, you can use this to open a shell and run any commands as if you were using `pipenv run`.

- `pipenv install`

  - This will install all modules used within the project. It is important to do anytime you pull the repo.

- `pipenv install <module>`

  - Use this to install new modules. It will add the modules to the venv and automatically handle version control.

- `pipenv run manage.py 0.0.0.0:8000`

  - This will make sure the backend is locally accessible. Ie you can connect to it from another device on the lan.
